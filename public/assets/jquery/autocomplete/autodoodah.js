﻿/*jslint  browser: true, white: true, plusplus: true */
/*global $, countries */

$(function () {
    'use strict';

    var countriesArray = $.map(countries, function (value, key) { return { value: value, data: key }; });

    
    // Initialize ajax autocomplete:
    $('#autocomplete-ajax').autocomplete({
        serviceUrl: '/autocomplete/resources',
        groupBy: 'category',
        paramName: 'searchString',
        dataType: 'json',
        //lookup: countriesArray,
        lookupFilter: function(suggestion, originalQuery, queryLowerCase) {
            var re = new RegExp('\\b' + $.Autocomplete.utils.escapeRegExChars(queryLowerCase), 'gi');
            return re.test(suggestion.value);
        },
        onSelect: function(suggestion) {
            if (suggestion.url != '') {
                location.href = '/resource/' + suggestion.url;
            }
            $('#selection-ajax').html('You selected: ' + suggestion.value + ', ' + suggestion.data);

        },
        onHint: function (hint) {
            $('#autocomplete-ajax-x').val(hint);
        },
        onInvalidateSelection: function() {
            $('#selection-ajax').html('You selected: none');
        }
        ,
        transformResult: function(response) {

            return {
                suggestions: $.map(response.suggestions, function(data) {
                    return { value: data.value, data: data.id, url: data.url, groupBy: data.data.category};
                })
            };

        }

    });

});
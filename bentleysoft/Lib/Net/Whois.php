<?php
namespace Bentleysoft\Lib\Net;

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

class Whois
{
  /**
   * @var string
   */
  private $domain;

  /**
   * @var
   */
  private $TLDs;

  /**
   * @var
   */
  private $subDomain;

  /**
   * @var mixed
   */
  private $servers;

  /**
   * @var string
   */
  private static $info = null;

  private static $error = 0;

  protected static $nonDoms = ['.', 'localhost', '127.in-addr.arpa', '0.in-addr.arpa', '255.in-addr.arpa'];

  /**
   * @param string $domain full domain name (without trailing dot)
   */
  public function __construct($domain)
  {
    if (in_array($domain, self::$nonDoms)) {
      return null;
    }

    $this->domain = $domain;
    // check $domain syntax and split full domain name on subdomain and TLDs
    if (
        preg_match('/^([\p{L}\d\-]+)\.((?:[\p{L}\-]+\.?)+)$/ui', $this->domain, $matches)
        || preg_match('/^(xn\-\-[\p{L}\d\-]+)\.(xn\-\-(?:[a-z\d-]+\.?1?)+)$/ui', $this->domain, $matches)
    ) {
      $this->subDomain = $matches[1];
      $this->TLDs = $matches[2];
    } else
      throw new \InvalidArgumentException("Invalid $domain syntax");
    // setup whois servers array from json file
    $this->servers = json_decode(file_get_contents( __DIR__.'/whois.servers.json' ), true);

    $this->info();
    
  }

   /**
    * Create a Carbon instance from a DateTime one
    *
    * @param string $domain
    *
    * @return static
    */
    public static function instance($domain)
    {
      return new static($domain);
    }

  /**
   *  Try to guess the registrar
   *  It follows 'Registrar:' but it can be in one or two lines...
   *
   * @param string $domain
   * @return mixed|string
   */
    public static function registrar($domain='') {
      if (0 != self::$error) {
        return "No registrar information available";
      }

      if (in_array($domain, self::$nonDoms)) {
        return '';
      }

      $ret ='N/A';

      $output = array();
      preg_match("/Registrar:.*/", self::$info, $output);

      if (count($output)>0) {
        $ret = str_replace('Registrar:', '', $output[0]);
        $ret = str_replace(array("\n", "\n", "\t", "\r"), '', $ret);
      }

      if ($ret == '') {
        preg_match("/Registrar.*:\r\n.*/", self::$info, $output);

        if (count($output)>0) {
          $ret = str_replace('Registrar:', '', $output[0]);
          $ret = str_replace(array("\n", "\n", "\t", "\r"), '', $ret);
        }
      
      }
      return $ret;
    }

  public function expiresAt() {
    if (0 != self::$error) {
      return "No whois information available ".self::$error;
    }

    if ($this->domain == null || in_array($this->domain, self::$nonDoms)) {
      return '';
    }

    $ret = \Carbon\Carbon::parse(null);

    $output = array();
    
    preg_match("/Expiry date:.*/", self::$info, $output);

    if (count($output)>0) {
      $ret = str_replace('Expiry date:', '', $output[0]);
      $ret = str_replace(array("\n", "\n", "\t", "\r", " "), '', $ret);
    }

    if (count($output)==0) {
      preg_match("/Registrar Registration Expiration Date:.*/", self::$info, $output);
      if (count($output)>0) {
        $ret = str_replace('Registrar Registration Expiration Date:', '', $output[0]);
        $ret = str_replace(array("\n", "\n", "\t", "\r" ," "), '', $ret);
      }
    }

    if (count($output)==0) {
      preg_match("/Registry Expiry Date:.*/", self::$info, $output);
      if (count($output)>0) {
        $ret = str_replace('Registry Expiry Date:', '', $output[0]);
        $ret = str_replace(array("\n", "\n", "\t", "\r" ," "), '', $ret);
      }
    }

    // convert to datetime
    if ($ret<>'N/A') {
      return \Carbon\Carbon::parse($ret);

    }

    return $ret;
  }



  public function info()
  {
    if (in_array($this->domain, self::$nonDoms)) {
      return null;
    }

    if ($this->isValid()) {

        $whois_server = $this->servers[$this->TLDs][0];

        // If TLDs have been found
        if ($whois_server != '') {

          // if whois server serve replay over HTTP protocol instead of WHOIS protocol
          if (preg_match("/^https?:\/\//i", $whois_server)) {

            // curl session to get whois reposnse
            $ch = curl_init();
            $url = $whois_server . $this->subDomain . '.' . $this->TLDs;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

            $data = curl_exec($ch);

            if (curl_error($ch)) {
              self::$info = "Connection error!";
              self::$error = 1;

              return "Connection error!";
            } else {
              $string = strip_tags($data);
            }
            curl_close($ch);

          } else {
            $string = \Cache::get($this->domain, null);
            if ($string==null) {
              // Getting whois information
              $fp = fsockopen($whois_server, 43);
              if (!$fp) {
                self::$info = "Connection error!";
                self::$error = 1;

                return "Connection error!";
              }

              $dom = $this->subDomain . '.' . $this->TLDs;
              fputs($fp, "$dom\r\n");

              // Getting string
              $string = '';

              // Checking whois server for .com and .net
              if ($this->TLDs == 'com' || $this->TLDs == 'net') {
                while (!feof($fp)) {
                  $line = trim(fgets($fp, 128));

                  $string .= $line;

                  $lineArr = explode (":", $line);

                  if (strtolower($lineArr[0]) == 'whois server') {
                    $whois_server = trim($lineArr[1]);
                  }
                }
                // Getting whois information
                $fp = fsockopen($whois_server, 43);
                if (!$fp) {
                    self::$info = "Connection error!";
                    self::$error = 1;
        
                    return "Connection error!";
                }

                $dom = $this->subDomain . '.' . $this->TLDs;
                fputs($fp, "$dom\r\n");

                // Getting string
                $string = '';

                while (!feof($fp)) {
                  $string .= fgets($fp, 128);
                }

                // Checking for other tld's
              } else {
                while (!feof($fp)) {
                  $string .= fgets($fp, 128);
                }
              }
              fclose($fp);
              \Cache::put($this->domain, $string, 60);
            }
          }


        $string_encoding = mb_detect_encoding($string, "UTF-8, ISO-8859-1, ISO-8859-15", true);
        $string_utf8 = mb_convert_encoding($string, "UTF-8", $string_encoding);

        self::$info = htmlspecialchars($string_utf8, ENT_COMPAT, "UTF-8", true);


        return htmlspecialchars($string_utf8, ENT_COMPAT, "UTF-8", true);
      } else {
        self::$info = "No whois server for this tld in list!";
        self::$error = 3;

        return self::$info;
      }
    } else {
      self::$info = "Domainname isn't valid!";
      self::$error = 2;

      return self::$info;
    }
  }

  public function htmlInfo()
  {
    return nl2br($this->info());
  }

  /**
   * @return string full domain name
   */
  public function getDomain()
  {
    return $this->domain;
  }

  /**
   * @return string top level domains separated by dot
   */
  public function getTLDs()
  {
    return $this->TLDs;
  }

  /**
   * @return string return subdomain (low level domain)
   */
  public function getSubDomain()
  {
    return $this->subDomain;
  }

  public function isAvailable()
  {
    $whois_string = $this->info();
    $not_found_string = '';
    if (isset($this->servers[$this->TLDs][1])) {
      $not_found_string = $this->servers[$this->TLDs][1];
    }

    $whois_string2 = @preg_replace('/' . $this->domain . '/', '', $whois_string);
    $whois_string = @preg_replace("/\s+/", ' ', $whois_string);

    $array = explode (":", $not_found_string);
    if ($array[0] == "MAXCHARS") {
      if (strlen($whois_string2) <= $array[1]) {
        return true;
      } else {
        return false;
      }
    } else {
      if (preg_match("/" . $not_found_string . "/i", $whois_string)) {
        return true;
      } else {
        return false;
      }
    }
  }

  public function isValid()
  {
    if (
        isset($this->servers[$this->TLDs][0])
        && strlen($this->servers[$this->TLDs][0]) > 6
    ) {
      $tmp_domain = strtolower($this->subDomain);
      if (
          preg_match("/^[a-z0-9\-]{3,}$/", $tmp_domain)
          && !preg_match("/^-|-$/", $tmp_domain) //&& !preg_match("/--/", $tmp_domain)
      ) {
        return true;
      }
    }

    return false;
  }
}
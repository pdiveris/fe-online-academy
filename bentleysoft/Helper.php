<?php namespace Bentleysoft;

use Closure;

class Helper
{
  /**
   * Paginator supported page sizes.. Just a helper
   * @return array
   */
  public static function pageSizes() {
    return array(10=>10,25=>25,50=>50,100=>100);
  }

}


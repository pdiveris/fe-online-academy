<?php namespace Bentleysoft\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use App\Http\Requests;

use Dingo\Api\Routing\Helpers;
use Dingo\Api\Http;
use Bentleysoft\Models;
use League\Flysystem\Exception;
use Symfony\Component\HttpKernel;

/**
 * Class AcademyController
 * @package Bentleysoft\Http\Controllers
 */
class AcademyController extends BaseController
{
  use Helpers;

  /**
   * @var \Illuminate\Http\Request
   */
  private $request;

  private $method = 'get';

  private $resource = 'course';

  private $resourceId = -1;

  private $allowedParams = [
    'format',
    'where',
    'orderBy',
    'expands',

  ];

  /**
   * json encoded wheres
   * @var array
   * @TODO: hierarhical wheres
   */
  private static $where = [];

  /**
   * json encoded order bys
   * @var array
   */
  private static $orderBy = [];

  /**
   * Items to expand - json encoded
   * @var array|mixed
   */
  private static $expands = [];

  /**
   * AcademyController constructor.
   *
   * @param \Dingo\Api\Http\Request $request
   *
   * @todo Check that resource and the corresponding method exist
   */
  public function __construct(Http\Request $request) {
    $this->request = $request;

    // build where params
    $params = [];
    $jWhere = $this->request->get('where','{}');
    $where = json_decode($jWhere);

    foreach ($where as $key=>$object ) {
      $arr = (array)$object;
      $keys = array_keys($arr);

      $params[] = array(
        'field'=>$key,
        'operand'=>$keys[0],
        'value'=>$arr[$keys[0]],
      );

    }

    self::$where = $params;
    // get expand courses,areas etc

    $expands = json_decode($this->request->get('expand','[]'));
    if (sizeof((array)$expands)>0) {
      foreach ($expands as $key => $object) {
        self::$expands[$key] = $object;
      }
    }

    self::$expands = $expands;
  }

  /**
   * @param array $params
   * @return string
   */
  public function missingMethod($params = []) {
    return 'missingMethod';
  }

  /**
   * @param $id
   * @return mixed
   */
  public function getUser($id)
  {
    if ($id>0) {
      $model =  Models\User::where('id','=',$id)
        ->get();

      if ($model && count($model)>0) {
        return $model[0];
      } else {
        throw new HttpKernel\Exception\NotFoundHttpException("User resource with id {$id} not found.");
      }

      return $model;
    } else {
      $models = Models\User::where('id','>',0)->get();
      return $models;
    }

    throw new HttpKernel\Exception\NotFoundHttpException("User not found.");
  }


  /**
   * @param $id
   * @return mixed
   * Here and now logic needs implementing
   * Fow now we know that depth should be  0 (?)
   */
  public function getCategory($id = 0) {
    if ($id>0) {
      $model =  Models\CourseCategory::where('id','=',$id)
        ->where('depth','=',1)
        ->get();

      if ($model && count($model)>0) {
        return $model[0];
      } else {
        throw new HttpKernel\Exception\NotFoundHttpException("Category resource with id {$id} not found.");
      }

      return $model;
    } else {

      $builder = Models\CourseCategory::where('parent','=',0)
        ->orderBy('sortorder', 'desc');

      // add the params
      foreach(self::$where as $key=>$value) {
        $builder->where($value['field'],$value['operand'],$value['value']);

      }

      $models = $builder->get();
      return $models;
    }

    throw new HttpKernel\Exception\NotFoundHttpException("Resource not found.");
  }


  /**
   * Let's follow old patterns cand call the second level 'subjects' (i.e., Subject Areas)
   * @param $id
   * @return mixed
   */
  public function getSubject($id = 0) {
    if ($id>0) {
      $model =  Models\CourseCategory::where('id','=',$id)
        ->where('depth','=',2)
        ->get();

      if ($model && count($model)>0) {
        ////$overview =
        return $model[0];
      } else {
        throw new HttpKernel\Exception\NotFoundHttpException("Subject resource with id {$id} not found.");
      }
    } else {

      $builder = Models\CourseCategory::where('depth','=',2)
        ->orderBy('sortorder');

      // add the params
      foreach(self::$where as $key=>$value) {
        $builder->where($value['field'],$value['operand'],$value['value']);

      }

      $models = $builder->get();

      /**
       * do the expands
       * @todo: move to function
       * @todo: hierarchy
       */
      if (count(self::$expands)>0) {
        foreach  ( $models as $model) {
          self::$where = [];

          foreach (self::$expands as $expand=>$scope) {
            $class = str_singular($expand);

            $func = $this->method . ucfirst($class);

            $model['method']= $func;

            self::$where[] = [
              'field'=>'category',
              'operand'=>'=',
              'value'=>$model->id,
            ];

            $model->$expand = $this->$func(0);
          }
        }

      }

      return $models;

    }

    throw new HttpKernel\Exception\NotFoundHttpException("Resource not found.");
  }
  /**
   * @param $id
   * @return mixed
   *
   * @TODO:
   * Add parameters for category, id number, format
   *
   */
  public function getCourse($id=0) {

    if ($id>0) {
      $builder =  Models\Course::where('id','=',$id);

      $model = $builder->get();

      if ($model && count($model)>0) {
        return $model[0];
      } else {
        throw new HttpKernel\Exception\NotFoundHttpException("Course resource with id {$id} not found.");
      }
    } else {

      $builder = Models\Course::where('id','>',0);

      // add the params
      foreach(self::$where as $key=>$value) {
        $builder->where($value['field'],$value['operand'],$value['value']);

      }

      // get the models...
      $models = $builder->get();

      if (sizeof(self::$expands)>0) {
        foreach  ( $models as $model) {

          foreach (self::$expands as $expand => $scope) {
            $class = str_singular($expand);
            $func = $this->method . ucfirst($class);
            $model['method'] = $func;


            self::$where[] = [
              'field'=>'category',
              'operand'=>'=',
              'value'=>$models[0]->id,
            ];

            ///$model->$expand = $this->$func(0);

          }
        }

      }

      return $models;
    }

    throw new HttpKernel\Exception\NotFoundHttpException("Resource not found.");
  }


  /**
   * @param int $id
   * @return mixed|\Illuminate\Http\Response
   */
  public function getReviews($id = 0) {
    if ($id>0) {
      $reviews = Models\FEOA\Review::where('resource_id','=',$id)->get();
      return $reviews;
    }
    throw new HttpKernel\Exception\NotFoundHttpException("Resource not found.");

  }


  public function putReview($id) {

  }

  /**
   * @param int $id
   * @return mixed|\Illuminate\Http\Response
   */
  public function getScores($id = 0) {
    if ($id>0) {
      $totalScore = Models\FEOA\ResourceScore::where('resource_id','=',$id)->first();
      $reviews = Models\FEOA\Review::where('resource_id','=',$id)->get();
      $scores = [];

      if ($totalScore && null != $totalScore) {
        $scores['resource_id'] = intval($totalScore->resource_id);

        $scores['count'] = intval($totalScore['c']);
        $scores['totalScore'] = floatval($totalScore['score']);

        foreach ($reviews as $review) {
          $scores['scores'][] = floatval($review->score);
        }
      }
      return $scores;
    }
    throw new HttpKernel\Exception\NotFoundHttpException("Resource not found.");

  }

  /************************************************* STORE API ****************************************************/

  /**
   * @param string $id
   * @return \Psr\Http\Message\StreamInterface
   */
  public function getStoreResources($id = '') {
    // $results = $this->api->get('v1/subject?where={"parent":{"=":"2"}}&order={"al"}&expand={"courses":"all"}');
    $suffix  = '';
    if ($id<>'') {
      $suffix = '/'.$id;
    }

    $uri = 'http://store.data.alpha.jisc.ac.uk:8080/resources'.$suffix;

    $client = new \GuzzleHttp\Client();
    $res = $client->request('GET', $uri, [

    ]);
    return $res->getBody();

  }


  /************************************************* STORE API ****************************************************/

  /**
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return ''; // return nothing;
  }

  /**
   * @param string $resource
   * @param int $id
   * @return mixed
   * @throws Exception
   */
  public function api($resource = '', $id = -1) {
    $params = [];
    $jWhere = $this->request->get('where','{}');
    $where = json_decode($jWhere);


    foreach ($where as $key=>$object ) {
      $arr = (array)$object;
      $keys = array_keys($arr);

      $params[] = array(
        'field'=>$key,
        'operand'=>$keys[0],
        'value'=>$arr[$keys[0]],
      );

    }

    if ($resource == '') {
      throw new Exception("Sorry something went wrong, no resource type was specified");
    }

    $resource = str_singular($this->request->segments()[2]);  // i.e. 'category', 'course' etc....

    // set method from request

    $func = $this->method . ucfirst($resource);

    if (method_exists($this, $func)) {
      return $this->$func($id, $params);
    } else {

    }


  }


}
<?php namespace Bentleysoft\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support;
use App\Http\Requests;

use Dingo\Api\Routing\Helpers;
use Dingo\Api\Http\Response;
use Bentleysoft\Models;
use Cartalyst\Sentry;
use Illuminate\Support\MessageBag;
use Symfony\Component\HttpKernel;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * Class ContentController
 * @package Bentleysoft\Http\Controllers
 */
class ContentController extends BaseController
{
  use Helpers;

  /**
   * @var \Illuminate\Http\Request
   */
  private $request;

  /**
   * @var array
   */
  private $areas = ['find-and-reuse'=>'find-and-reuse', 'build'=>'build', 'learn'=>'learn'];

  /**
   * @var string
   */
  private static $layouts ='';

  /**
   * @var null
   */
  private static $user = null;

  /**
   * @var null
   */
  static $digitalCapability = null;

  /**
   * @param \Illuminate\Http\Request $request
   *
   * @todo Check that resource and the corresponding method exist
   */
  public function __construct(Request $request) {

    if (null==self::$user);

    $this->request = $request;
    if (isset($request->ui) && $request->ui=='material') {
      self::angulariseTags();
    } else {

    }

  }

  /**
   *  Switch Laravel's Blade tags so they don't conflict with Angular
   */
  private static function angulariseTags() {
    \Blade::setRawTags("[[", "]]");
    \Blade::setContentTags('<%', '%>'); // for variables and all things Blade
    \Blade::setEscapedContentTags('<%%', '%%>'); // for escaped data

  }

  public static function getLayouts() {
    return self::$layouts;
  }

  /**
   * @return \Illuminate\View\View
   */
  public function getHome($material = '') {
    $areas = $this->api->get('v1/category');
    return view(self::$layouts.'welcome', ['areas'=>$areas]);
  }

  /**
   * @return \Illuminate\View\View
   */
  public function getMaterialHome($material = '') {
    $areas = $this->api->get('v1/category');

    return view('material.welcome', ['areas'=>$areas]);
  }

  /**
   * @param string $area
   * @return \Illuminate\View\View
   */
  public function getArea($area = '') {
    if (!array_key_exists($area, $this->areas)) {
      throw new HttpKernel\Exception\NotFoundHttpException("Ouch. Can't find this!");
    }

    $result = $this->api->get('v1/category?where={"name":{"=":"'.$this->unslug($area).'"}}');
    return view(self::$layouts.$area, ['data'=>$result]);

  }


  /**
   * @param $request
   * @param string $area
   * @return \Illuminate\View\View
   */
  public function getMaterialArea($request, $area = '') {

    if (!array_key_exists($area, $this->areas)) {
      throw new HttpKernel\Exception\NotFoundHttpException("Ouch. Can't find this!");
    }

    $result = $this->api->get('v1/category?where={"name":{"=":"'.$this->unslug($area).'"}}');

    return view("material.$area", ['data'=>$result, 'request'=>$request]);

  }

  public function getLogout() {
    // Logs the user out

    $sentry = new \Cartalyst\Sentry\Sentry;

    $sentry->logout();
    // $sentry->clear();
    return \Redirect::back();

  }

  /**
   * Method to render and proces the password change form
   * @version 1.0
   * @todo compare password1 and password2
   */
  public function changePassword()
  {
    $view = 'password';
    $output = view($view, []);

    $rules = array('email' => array('required'), 'password' => array('required'), 'password2' => array('required', 'same:password'), '');

    $validator = \Validator::make(\Input::all(), $rules, array('required' => 'The :attribute is required.'));

    if (\Request::getMethod() == 'POST') {

      if ($validator->fails()) {
        $messages = $validator->messages();
        return \Redirect::back()->withInput()->withErrors($messages);
      } else {

        try {
          $code = \Input::get('code');
          $email = filter_var(\Input::get('email'), FILTER_SANITIZE_EMAIL);
          $password = \Input::get('password');

          $sentry = new Sentry\Sentry();

          $user = $sentry->findUserByCredentials(array(
            'email' => $email
          ));

          if ($user->checkResetPasswordCode($code)) {
            if ($user->attemptResetPassword($code, $password)) {
              return \Redirect::back()->withInput()->with(array('success' => 'Your password has now been changed.'));
            } else {
              throw new \Exception('Sorry, something went wrong. Your password could not be reset.');
            }
          } else {
            throw new \Exception('Sorry, something went wrong. Your password could not be reset.');
          }
        } catch (\Exception $e) {
          echo $e->getMessage();
          exit;
        }

      }

    }

    return $output;
  }

  /**
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function getResetPassword() {
    return view("reset", []);
  }

  /**
   * @return mixed
   */
  public function postResetPassword() {

    $email = \Input::get('email');

    $rules = array('email' => array('required'));

    $validator = \Validator::make(\Input::all(), $rules, array('required' => 'The :attribute is required.'));


    if ($validator->fails()) {
      $messages = $validator->messages();

      return \Redirect::back()->withInput()->withErrors($messages);
    }

    try {
      $sentry = new Sentry\Sentry;

      $user = $sentry->findUserByCredentials(array(
        'email' => $email
      ));

      $code = $user->getResetPasswordCode();

      $data = array('code' => $code, 'user' => $user, 'resetUrl' => url('/login/change'));

      \Mail::send('emails.reset', array('data' => $data), function ($message) use ($user, $code) {

        $from = ['email'=>'system@bentleysoft.com', 'name'=>'FE and Skills Online Academy' ];

        $fromEmail = $from['email'];
        $fromName = $from['name'];

        $message->to($user->email, $user->firstname . ' ' . $user->lastname)->subject('Your password reset code ');
        $message->from($fromEmail, $fromName);
      });

      return \Redirect::back()->withInput()->with(array('success' => 'Your password reset code has been sent and should be with you shortly'));
    } catch (\Exception $e) {

      return redirect('login/reset')
        ->withErrors(['error'=>"A user with this email address was not found, sorry.."])
        ->withInput();
    }

  }

  /**
   * @param Request $request
   * @param string $section
   * @return string
   */
  public static function active(Request $request, $section = '') {
    $ret = '';

    if ( $section<>'') {
      $paths = explode('/',$request->path());
      if (count($paths)>0) {
        if ($paths[count($paths)-1] == $section || $section=='find-and-reuse' && $paths[0]=='resource') {
          $ret = 'active';
        }
      }
    }
    return $ret;
  }

  /**
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function getLogin() {
    return view("login", ['slug'=>\Input::get('resource','')]);
  }

  /**
   * @return mixed
   * @throws \Exception
   */
  public function postLogin() {
    $rules = array('email' => array('required'), 'password' => array('required'));
    $validator = \Validator::make(\Input::all(), $rules, array('required' => 'The :attribute is required.'));

    if ($validator->fails()) {
      $messages = $validator->messages();

      return \Redirect::back()->withInput()->withErrors($messages);
    } else {
      try {
        // Set login credentials
        $credentials = array(
          'email' => \Input::get('email'),
          'password' => \Input::get('password'),
        );

        // Try to authenticate the user
        $sentry = new Sentry\Sentry;

        // $user = Sentry\Sentry::authenticate($credentials, false);
        $user = $sentry->authenticate($credentials, false);

        if ($user) {
          $slug = \Input::get('slug', '');
          if ($slug<>'') {
            return \Redirect::to(url('resource/' . \Input::get('slug')));
          } else {
            return \Redirect::to(url('area/find-and-reuse'));
          }
        } else {
          throw new \Exception(500, "Big fat error");
        }

      } catch (Sentry\Users\WrongPasswordException $e) {

        return \Redirect::back()->withInput()->withErrors(['error'=>'Wrong password, please try again']);

      } catch (Sentry\Users\UserNotFoundException $e) {

        $messageBag = new MessageBag;
        $messageBag->add('errors', 'Unknown user');

        return \Redirect::back()->withInput()->withErrors($messageBag);

      } catch (Sentry\Users\UserNotActivatedException $e) {
        echo 'User is not activated.';
      }
    }
  }

  /**
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function getJwt() {
    return view("jwt", ['slug'=>\Input::get('resource','')]);
  }

  /**
   * @return mixed
   * @throws \Exception
   */
  public function postJwt() {

    $rules = array('email' => array('required'), 'password' => array('required'));
    $validator = \Validator::make(\Input::all(), $rules, array('required' => 'The :attribute is required.'));

    if ($validator->fails()) {
      $messages = $validator->messages();

      return \Redirect::back()->withInput()->withErrors($messages);
    } else {

      // grab credentials from the request
      $credentials =\Input::only('email', 'password');

      try {
        // attempt to verify the credentials and create a token for the user
        if (! $token = JWTAuth::attempt($credentials)) {
          return response()->json(['error' => 'invalid_credentials'], 401);
        }
      } catch (JWTException $e) {
        // something went wrong whilst attempting to encode the token
        return response()->json(['error' => 'could_not_create_token'], 500);
      }

      // all good so return the token
      return response()->json(compact('token'));


    }

  }

  /**
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function getJoin() {
    return view("join", ['slug'=>\Input::get('resource','')]);
  }

  /**
   * @return mixed
   * @throws \Exception
   */
  public function postJoin() {
    $rules = ['firstname' =>['required'],
              'lastname'=>['required'],
              'email' =>['required'],
              'password' => ['required','confirmed','min:6']

    ];
    $validator = \Validator::make(\Input::all(), $rules, array('required' => 'The :attribute is required.'));

    if ($validator->fails()) {
      $messages = $validator->messages();

      return \Redirect::back()->withInput()->withErrors($messages);
    } else {
      return \Redirect::back()->with(['success'=>'You review has been posted and will be published shortly', 'scrollTop'=>'yes']);

    }

  }

  /**
   * Decorate the Form label with a Jisc UX pattern to reflect its error status
   *
   * @param string $field
   * @param Support\ViewErrorBag $viewBag
   * @return string
   */
  public static function getClassForLabel($field = '', Support\ViewErrorBag $viewBag) {

    if ($viewBag->hasBag()) {
      $messageBag = $viewBag->getBag('default');

      if ($field !== '' && $messageBag->has($field)) {
        return 's-error';
      }
    }
    return 'kosher';
  }

  /**
   * @return mixed
   */
  public function postReview() {

    $sentry = new Sentry\Sentry;
    $user = $sentry->getUser();

    $rules = array('title'=>['required'], 'comments' => ['required']);

    $validator = \Validator::make(\Input::all(), $rules, ['title.required' => 'A summary is required.', 'comments.required'=>'Comments are required.'] );

    if ($validator->fails()) {
      $messages = $validator->messages();

      return \Redirect::back()->withInput()->withErrors($messages);
    } else {
      $model = new Models\FEOA\Review;

      $model->user_id = $user->getId();

      $model->title = \Input::get('title');
      $model->comments = \Input::get('comments');

      $model->score = \Input::get('score');

      $model->resource_id = \Input::get('resource_id');
      $model->save();


      return \Redirect::back()->with(['success'=>'You review has been posted and will be published shortly', 'scrollTop'=>'yes']);

    }

  }

  /**
   * Respond to GET /resource/{url}
   *
   * @param string $slug
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function getResource($slug = '') {
    $sentry = new Sentry\Sentry;
    $user = $sentry->getUser();

    // unslug. this needs working (three calls, as is, with dashes to spaces and with dashes to underscores
    $slug = str_replace('_','%',$slug);
    $slug = str_replace('-','%',$slug);

    $resource = $this->api->get('v1/course/?where={"shortname":{"like":"'.urlencode($slug).'"}}');

    if (!$resource) {
      abort(404,"Ouch! Can't find the resource you are looking for..");
    }
    $labels = Models\Label::where('course','=',$resource[0]->id)->get();

    $overview = Models\CourseSection::where('course','=',$resource[0]->id)
      ->where('name','=','overview')
      ->get();

    if (count($overview)>0) {
      $overview[0]->summary = Models\File::replaceMediaReferences($overview[0]->summary);
    }

    if (count($labels)>0) {
      foreach($labels as $label) {
        $label->intro =  Models\File::replaceMediaReferences($label->intro);
      }
    }

    //$result = $this->api->get('v1/subject?where={"id":{"=":"'.'12'.'"}}');
    $result = [];

    $reviews = $this->api->get('v1/reviews/'.$resource[0]->id);

    $scores = (object)$this->api->get('v1/scores/'.$resource[0]->id);


    return view(self::$layouts.'resource',
                  ['result'=>$result,
                   'resource'=>$resource[0],
                   'meta_title'=>$resource[0]->fullname,
                   'labels'=>$labels,
                   'scores'=>$scores,
                   'user'=>$user,
                   'reviews'=>$reviews,
                   'overview'=>$overview]);
  }

  /**
   * @param string $slug
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   *
   * @TODO: The Store API doesn't support where clauses at the moment where={'id': ... }
   * @TODO: Store API SORT_ORDER and LIMIT support
   *
   * @TODO: Unhardcode the Reviews
   * @TODO: Unhardcode the Scores
   */
  public function getStoreItem($slug = '') {
    $sentry = new Sentry\Sentry;
    $user = $sentry->getUser();

    $json = (string)$this->api->get('v2/store/resources/'.$slug);
    $resource = json_decode($json);
    $resource = $resource->resource;


    $reviews = $this->api->get('v1/reviews/11');

    $scores = (object)$this->api->get('v1/scores/11');

    return view(self::$layouts.'store',
      ['result'=>[],
        'resource'=>$resource,
        'meta_title'=>$resource->title,
        'labels'=>[],
        'scores'=>$scores,
        'user'=>$user,
        'reviews'=>$reviews,
        'overview'=>[]]);
  }

  /**
   * @param string $id
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function getMaterialResource($id = '') {
    // $resolver = Models\Resolver::where('cat_id','=',$id)->first();
    $resolver = Models\Resolver::where('cat_id','>',0)
      ->first();

    $content = Models\Content::findOrNew($resolver->content_id);

    $result = $this->api->get('v1/subject?where={"id":{"=":"'.$id.'"}}');

    return view('material.resource', ['result'=>$result, 'content'=>$content]);

  }

  /**
   * @param string $query
   * @return bool
   */
  public function getCourse($query = '') {

    $result = $this->api->get('v1/course?where={"fullname":{"like":"'.$query.'%"}}');

    var_dump($result);
    return false;
  }

  /**
   * Try your best to turn a URL slug into word(s)
   * @param $string
   * @return mixed
   */
  public function unslug($string) {
    $ret = str_replace('-', ' ', $string);

    return $ret;

  }


  /**
   * Asynchronous data feeder (don't use the word AJAX, it's banned)
   * @param string $query
   * @return array
   */
  public function getAutocomplete($query = '')
  {
    /*
     * Get 1nd level cats (areas)
     */

    // $results = $this->api->get('v1/subject?where={"parent":{"=":"2"}}&order={"al"}&expand=courses');

    $suggestions = [];

    $v2 = $_ENV['APP_API_MOODLE']<>'true';

    if ($v2) {
      $result = $this->api->get('v2/store/resources');
      $json = (string) $result;

      $resources = json_decode($json);

      foreach ($resources->data as $i=>$resource) {
        $suggestions[] = [
          'data' => ['category' =>'Jisc Store'],
          'value' => $resource->title,
          'kind' => 'store',
          'url' => str_slug($resource->id),
        ];
      }

    } else {

      $results = $this->api->get('v1/subject?where={"parent":{"=":"2"}}&order={"al"}&expand={"courses":"all"}');

      foreach ($results as $i => $result) {
        if (count($result->courses) > 0) {
          foreach ($result->courses as $course) {
            $suggestions[] = [
              'data' => ['category' => $result->name . ' (' . count($result->courses) . ')'],
              'value' => $course->fullname,
              'kind' => 'vle',
              'url' => str_slug($course->shortname),
            ];

          }
        }
      }
    }

    $ret = [
      "query"=>"Unit",
      'paramName'=>'query',
      "suggestions"=>$suggestions,
    ];

    return $ret;
  }




  public function getResourceForMaterial($slug = '') {

    $sentry = new Sentry\Sentry;
    $user = $sentry->getUser();

    // unslug. this needs working (three calls, as is, with dashes to spaces and with dashes to underscores
    $slug = str_replace('_','%',$slug);
    $slug = str_replace('-','%',$slug);

    $resource = $this->api->get('v1/course/?where={"shortname":{"like":"'.urlencode($slug).'"}}');


    if (!$resource) {
      abort(404,"Ouch! Can't find the resource you are looking for..");
    }

    $labels = Models\Label::where('course','=',$resource[0]->id)->get();

    $overview = Models\CourseSection::where('course','=',$resource[0]->id)
      ->where('name','=','overview')
      ->get();

    if (count($overview)>0) {
      $overview[0]->summary = Models\File::replaceMediaReferences($overview[0]->summary);
    }

    if (count($labels)>0) {
      foreach($labels as $label) {
        $label->intro =  Models\File::replaceMediaReferences($label->intro);
      }
    }


    $reviews = Models\FEOA\Review::where('resource_id','=',$resource[0]->id)->get();

    $score = 0;

    $resourceScore = Models\FEOA\ResourceScore::where('resource_id','=',$resource[0]->id)->first();

    if ($resourceScore) {
      $score = $resourceScore->score;
    }

    $ret = [
      'resource'=>$resource,
      'overview'=>$overview,
      'labels'=>$labels,
      'score'=>$score,
      'reviews'=>$reviews,
    ];

    return $ret;

  }

  /**
   * @param string $query
   * @return array
   */
  public function getMaterialAutocomplete($query = '')
  {
    /*
     * Get 1nd level cats (areas)
     */
    $results = $this->api->get('v1/subject?where={"parent":{"=":"2"}}&order={"al"}&expand={"courses":"all"}');

    $suggestions = [];

    foreach ($results as $i => $result)
    {
      if (count($result->courses)>0) {
        foreach($result->courses as $course) {
          $suggestions[] = [
            'data'=>['category'=>$result->name . ' ('.count($result->courses).')'],
            'value'=>$course->shortname,
            'display'=>$course->fullname,
            'url'=> str_slug($course->shortname),
          ] ;

        }
      }
    }

    $ret = [
      "query"=>"Unit",
      'paramName'=>'query',
      "suggestions"=>$suggestions,
    ];

    return $ret;
  }




  /**
   * /mcdn/179/6995ec4225ba088401fb221191f9f8bbe2fffcae/Inst%20Motoring.png
   * 69 95 6995ec4225ba088401fb221191f9f8bbe2fffcae -
   *
   * @param string $context
   * @param string $hash
   * @param string $fileName
   * @return mixed
   */
  public function moodleCDN($context='', $hash='', $fileName=''){

    if ($context<>'') {

    }

    $moodleData = '/var/moodledata';

    $x = substr($hash,0,2);
    $y = substr($hash,2,2);

    $path = "$moodleData/filedir/$x/$y/$hash";

    $lastModified = @filectime($path);

    $age = 60*60*24*7;

    $expires = gmdate('D, d M Y H:i:s', time()+$age).' GMT';

    $eTag = md5($lastModified.$fileName);

    $modifiedSince = $this->request->server->get('HTTP_IF_MODIFIED_SINCE');
    $noneMatch = $this->request->server->get('HTTP_IF_NONE_MATCH');

    $gmtMtime = gmdate('r', $lastModified);

    $mimeType = \GuzzleHttp\Psr7\mimetype_from_filename($fileName);

    if ($modifiedSince == $gmtMtime || str_replace('"', '', stripslashes($noneMatch)) == md5($lastModified.$fileName)) {
      return response('Petros Diveris')
        ->header('Status', '304');
    }

    $content = file_get_contents("$moodleData/filedir/$x/$y/$hash");
    $length = strlen($content);

    return (new Response($content, '200'))
      ->header('Cache-Control', "private, max-age=$age")
      ->header('Expires', $expires)
      ->header('Content-Length', $length)
      ->header('Last-Modified', $lastModified)
      ->header('ETag', $eTag)
      ->header('Content-Type', $mimeType);

  }

  /**
   * @return mixed
   */
  public static function getDiagnosticToolQuestions() {
    $x = 'pako was here';

    if (!null==self::$digitalCapability) {
      return self::$digitalCapability;
    }

    // get a client
    $client = new \GuzzleHttp\Client();

    // do a get request
    $res = $client->request('GET', 'http://diagnostictoolbuilder.azurewebsites.net/api/toolkits/json/56602e28efb0b32873f6149f/', []);

    $json =  (string)$res->getBody();
    $dt = json_decode($json);

    $dc = new Models\DT\DigitalCapability((array)$dt);
    if (null==self::$digitalCapability) {
      self::$digitalCapability = $dc;
    }

    return $dc;
  }

  // POST TO /api/quiz
  public function postAreaLearn() {
    /*
    // get a client
    $client = new \GuzzleHttp\Client();
    // do a get request
    $res = $client->request('POST', 'http://diagnostictoolbuilder.azurewebsites.net/api/quiz', [
        'form_params' => [
            \Input::except('_token')

        ]

    ]);

    $json =  (string)$res->getBody();
    $dt = json_decode($json);

    dd($dt);
    */
    $area = 'learn';
    return view(self::$layouts.$area, ['data'=>['DT'=>[

              ]
            ]
        ]);

  }

}
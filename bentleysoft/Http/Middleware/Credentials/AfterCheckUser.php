<?php namespace Bentleysoft\Http\Middleware\Credentials;

use Closure;
use Cartalyst\Sentry\Sentry;
use Bentleysoft\Http\Controllers\UserController;

class AfterCheckUser {

  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next)
  {
    
    if (null==\Bentleysoft\Http\Controllers\UserController::getUser()) {
      $sentry = UserController::getSentry();
      $user = $sentry->getUser();

      if (!null == $user) {
        UserController::setUser($sentry->getUser());
        if (!null==UserController::getMergedPermissions()) {
          // this just sets the permissions
        }
      }

    }

    return $next($request);
  }

}

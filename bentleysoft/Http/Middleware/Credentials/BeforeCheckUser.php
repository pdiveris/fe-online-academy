<?php namespace Bentleysoft\Http\Middleware\Credentials;

use Closure;
use Cartalyst\Sentry\Sentry;

class BeforeCheckUser {

  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   *
   */
  public function handle($request, Closure $next)
  {
    if ($request->path()<>$_ENV['prefix'].'/test' && $request->path()<>$_ENV['prefix'].'/user/login' && $request->path()<>$_ENV['prefix'].'/user/logout' && $request->path()<>$_ENV['prefix'].'/user/login/reset' && $request->path()<>$_ENV['prefix'].'/user/newpassword') {
      
      $sentry = new Sentry;
      if (!$sentry->check()) {
        return redirect($_ENV['prefix'].'/user/login');
      }
    }
    return $next($request);
  }

}

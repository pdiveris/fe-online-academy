<?php namespace Bentleysoft\Http\Middleware\Curiosity;

use Closure;
/**
* Does frack all, it's just so that I get to learn Lumen's Middleware....
*/
class AfterCleanup {

  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next)
  {
    return $next($request);
  }

}

<?php
namespace Bentleysoft\Http\Transformers;

class TestTransformer extends \League\Fractal\TransformerAbstract
{
  /**
   * @param \App\Models\Event $item
   *
   * @return array
   */
  public function transform($item)
  {
    return [
      'url'   => $item->url
    ];
  }
}
<?php namespace Bentleysoft\Models;
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 08/06/15
 * Time: 14:34
 *
 */

/*
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
*/

use Illuminate\Database\Eloquent\Model;

/**
 * Class Resolver
 * @package Bentleysoft\Models
 *
 * @property int id
 * @property int cat_id
 * @property int content_id
 * @property datetime created_at
 * @property datetime updated_at
 *
 */
final class Resolver extends Model {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'resolver';

  /**
   * The connection used with this model
   *
   * @var string
   */
  protected $connection = 'feoa';


}

<?php namespace Bentleysoft\Models;
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 08/06/15
 * Time: 14:34
 *
 */

/*
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
*/

use Illuminate\Database\Eloquent\Model;

/**
 * Class CourseCategory
 * @package Bentleysoft\Models
 *
 * @property int id
 * @property string name
 * @property string idnumber
 * @property string description
 * @property int descriptionformat
 * @property int parent
 * @property int sortorder
 * @property int coursecount
 * @property int visible
 * @property int visibleold
 * @property int timemodified
 * @property int depth
 * @property string path
 * @property string theme
 *
 */
final class CourseCategory extends Model {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'mdl_course_categories';


}

<?php namespace Bentleysoft\Models;
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 08/06/15
 * Time: 14:34
 *
 */

/*
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
*/

use DebugBar\DebugBar;
use Illuminate\Database\Eloquent\Model;

/**
 * Class File
 * @package Bentleysoft\Models
 *
 * @property int id
 * @property string contenthash     e.g. 68d750e2a59f234c8aebe5188fee8f8067fc74f1 with 68/d7/
 * @property string pathnamehash    e.g. 9e91f09eb29056a03e2d477d887868f93c43fe84
 * @property int contextid          e.g. 277
 * @property string component       e.g. mod_label
 * @property string filearea        e.g. intro
 * @property int itemid             e.g. 0
 * @property string filepath        e.g. /
 * @property string filename        e.g. car_batt.png
 * @property int userid             e.g. 19
 * @property int filesize           e.g. 837833
 * @property string mimetype        e.g. image/png
 * @property int status             e.g.  0
 * @property string source          e.g. car_batt.png
 * @property string author          e.g. Bob Myers
 * @property string license         e.g. allrightsreserved
 * @property datetime timecreated   e.g. 1436441352
 * @property datetime timemodified  e.g. 1436442463
 * @property int sortorder          e.g. 0
 * @property int referencefileid    e.g. null
 *
 */
final class File extends Model {

  /**
   * The database table used by the model.
   */

  /**
   * @var string
   */
  protected $table = 'mdl_files';

  /**
   * @param string $body
   * @return mixed
   */
  public static function replaceMediaReferences($body = '') {
    $pako = preg_replace_callback(
      '|src="([^"]*)"|',
      function ($matches) {
        if (isset($matches) && count($matches)>1)
          if (strpos($matches[1],'@@PLUGINFILE@@')!==false) {
            $fileName = str_replace('@@PLUGINFILE@@/','',$matches[1]);
          } else {
            $parts = explode('/',$matches[1]);
            $fileName = $parts[count($parts)-1];
          }

          $fileUrlish = $fileName;

          $fileName = urldecode($fileName);
          $file = self::where('filename','=',$fileName)->first();

        return "src=\"/mcdn/{$file->contextid}/{$file->contenthash}/{$fileUrlish}\"";
      },
      $body
    );
    return $pako;
  }
}

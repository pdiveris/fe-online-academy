<?php namespace Bentleysoft\Models;
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 08/06/15
 * Time: 14:34
 *
 */

/*
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
*/

use Illuminate\Database\Eloquent\Model;

/**
 * Class Course
 * @package Bentleysoft\Models
 *
 * @property int id
 * @property int category
 * @property int sortorder
 * @property string fullname
 * @property string shortname
 * @property string idnumber
 * @property string summary
 * @property int summaryformat
 * @property string format
 * @property boolean showgrades
 * @property int newsitems
 * @property int startdate
 * @property int marker
 * @property int maxbytes
 * @property int legacyfiles
 * @property boolean showreports
 * @property boolean visible
 * @property boolean visibleold
 * @property int groupmode
 * @property int groupmodeforce
 * @property int defaultgroupingid
 * @property string lang
 * @property string calendartype
 * @property string theme
 * @property int timecreated
 * @property int timemodified
 * @property boolean requested
 * @property boolean enablecompletion
 * @property boolean completionnotify
 * @property int cacherev
 *
 */
final class Course extends Model {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'mdl_course';


}

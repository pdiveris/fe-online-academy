<?php namespace Bentleysoft\Models;
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 08/06/15
 * Time: 14:34
 */

use Illuminate\Database\Eloquent\Model;

final class User extends Model {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'mdl_user';


}

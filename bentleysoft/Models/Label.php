<?php namespace Bentleysoft\Models;
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 08/06/15
 * Time: 14:34
 *
 */

/*
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
*/

use Illuminate\Database\Eloquent\Model;

/**
 * Class Label
 * @package Bentleysoft\Models
 *
 * @property int id
 * @property int course
 * @property string name
 * @property string intro
 * @property int introformat
 * @property ibt timemodified
 *
 */
final class Label extends Model {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'mdl_label';


}

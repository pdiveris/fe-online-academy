<?php namespace Bentleysoft\Models\FEOA;
/**
 * Created by Handi Craft.
 * User: Petros Diveris
 * Date: 2015-12-14
 * Time: 21:38
 *
 */

/*
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
*/

use Illuminate\Database\Eloquent\Model;

/**
 * Class Review
 * @package Bentleysoft\Models
 *
 * @property int id
 * @property int user_id
 * @property string title
 * @property string comments
 * @property int score
 * @property int resource_id
 * @property int created_at
 * @property int updated_at
 *
 */
final class Review extends Model {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'reviews';

  /**
   * The connection used with this model
   *
   * @var string
   */
  protected $connection = 'feoa';

  /**
   * @return \Illuminate\Database\Eloquent\Relations\HasOne
   */
  public function user()
  {
    return $this->hasOne('Bentleysoft\Models\FEOA\User', 'id', 'user_id');
  }


}

<?php namespace Bentleysoft\Models\FEOA;
/**
 * Created by Handi Craft.
 * User: Petros Diveris
 * Date: 2015-12-14
 * Time: 21:38
 *
 */

/*
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
*/

use Illuminate\Database\Eloquent\Model;

/**
 * Class ResourceScore
 * @package Bentleysoft\Models
 *
 * @property int resource_id
 * @property int count
 * @property float score
 *
 */
final class ResourceScore extends Model {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'resource_score';

  /**
   * The connection used with this model
   *
   * @var string
   */
  protected $connection = 'feoa';


}

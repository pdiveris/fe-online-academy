<?php namespace Bentleysoft\Models\FEOA;
/**
 * Created by Handi Craft.
 * User: Petros Diveris
 * Date: 2015-12-14
 * Time: 21:38
 *
 */

/*
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
*/

use Illuminate\Database\Eloquent\Model;

/**
 * Class User
 * @package Bentleysoft\Models
 *
 * @property int id
 * @property string firstname
 * @property string lastname
 * @property string password
 * @property string permissions
 * @property int activated
 * @property string activation_code
 * @property datetime activated_at
 * @property datetime last_login
 * @property string persist_code
 * @property string reset_password_code
 * @property datetime created_at
 * @property datetime updated_at
 *
 */
final class user extends Model {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'users';

  /**
   * The connection used with this model
   *
   * @var string
   */
  protected $connection = 'feoa';



}

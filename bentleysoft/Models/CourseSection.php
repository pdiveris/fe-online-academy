<?php namespace Bentleysoft\Models;
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 08/06/15
 * Time: 14:34
 *
 */

/*
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
*/

use Illuminate\Database\Eloquent\Model;

/**
 * Class CourseSection
 * @package Bentleysoft\Models
 *
 * @property int id
 * @property int course
 * @property int section
 * @property varchar name
 * @property varchar summary
 * @property int summaryformat
 * @property long sequence
 * @property int visible
 * @property varchar
 *
 */
final class CourseSection extends Model {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'mdl_course_sections';


}

## Digital Launchpad

[![Build Status](http://ci.data.alpha.jisc.ac.uk:3000/pdiveris/fe-online-academy/badge?branch=store)](http://ci.bentleysoft.com/build-status/view/3)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Digital Launchpad

## Official Documentation

Documentation for the launchpad can be found on the [DL website](http://www.jisc.ac.uk/docs).

## Contributing

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

### License

The Digital Launchpad is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
<?php

namespace App\Http\Middleware;

use Closure;

class FiltersMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        ///header('Access-Control-Allow-Origin: https://ux.jisc.ac.uk');        
        return $next($request);
    }
}

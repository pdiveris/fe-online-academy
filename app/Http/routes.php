<?php
use Dingo\Api\Routing\Helpers;
use Dingo\Api\Http\Response;
use JonnyW\PhantomJs\Client;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
| FEOA Moodle Branch (not merged)
*/
Route::get('/i', function() {
  phpinfo();
});

Route::get('/bench', function() {
  echo 'zak';
});

Route::get('/disqus', function() {

});

Route::get('/mcdn/{context?}/{hash?}/{name?}', '\Bentleysoft\Http\Controllers\ContentController@moodleCDN');

Route::get('/', '\Bentleysoft\Http\Controllers\ContentController@getHome');
Route::get('/area/{area?}', '\Bentleysoft\Http\Controllers\ContentController@getArea');
Route::get('/resource/{id?}', '\Bentleysoft\Http\Controllers\ContentController@getResource');

Route::get('/login/reset', '\Bentleysoft\Http\Controllers\ContentController@getResetPassword');
Route::post('/login/reset', '\Bentleysoft\Http\Controllers\ContentController@postResetPassword');

Route::get('/login/change', '\Bentleysoft\Http\Controllers\ContentController@changePassword');
Route::post('/login/change', '\Bentleysoft\Http\Controllers\ContentController@changePassword');

Route::get('/login', '\Bentleysoft\Http\Controllers\ContentController@getLogin');
Route::post('/login', '\Bentleysoft\Http\Controllers\ContentController@postLogin');
Route::get('/logout', '\Bentleysoft\Http\Controllers\ContentController@getLogout');

Route::get('/jwt', '\Bentleysoft\Http\Controllers\ContentController@getJwt');
Route::post('/jwt', '\Bentleysoft\Http\Controllers\ContentController@postJwt');


Route::get('/join', '\Bentleysoft\Http\Controllers\ContentController@getJoin');
Route::post('/join', '\Bentleysoft\Http\Controllers\ContentController@postJoin');

Route::post('/review', '\Bentleysoft\Http\Controllers\ContentController@postReview');

Route::get('/material/', function() {
  $this->app->request->ui = 'material';
  return App::make('\Bentleysoft\Http\Controllers\ContentController')->getMaterialHome($this->app->request);
});

Route::get('material/area/{area?}', function($area) {
  $this->app->request->ui = 'material';
  return App::make('\Bentleysoft\Http\Controllers\ContentController')->getMaterialArea($this->app->request, $area);
});

Route::get('/material/resource/{id?}', function($id) {
  $this->app->request->ui = 'material';
  return App::make('\Bentleysoft\Http\Controllers\ContentController')->getResourceForMaterial($id);
});


// this below was for debuffing. some serious cleaning has to take place here!
Route::get('/course/{query?}', '\Bentleysoft\Http\Controllers\ContentController@getCourse');

Route::get('/cats', function () {
  $cats = \Bentleysoft\Models\CourseCategory::where('parent','=',0)->get();

  return $cats;
});


Route::get('/dogs/{query?}', function($query = '') {
    return App::make('\Bentleysoft\Http\Controllers\ContentController')->getAutocomplete($query);

    }
);

Route::get('/fetch/{url?}', function($url) {
//  die($url);
  $browsershot = new Spatie\Browsershot\Browsershot();
    $browsershot
        ->setURL("http://$url")
        ->setWidth(1024)
        ->setHeight(768)
        ->setTimeout(5000)
        ->save('/home/apache/feoa.bentleysoft.com/feoa/public/shot.jpg');


  $img = Image::make('/home/apache/feoa.bentleysoft.com/feoa/public/shot.jpg')->resize(600, 400);

  return $img->response('jpg');

});


Route::get('/autocomplete/resources', function () {
  $query = $this->app->request->input('searchString');
  return App::make('\Bentleysoft\Http\Controllers\ContentController')->getAutocomplete($query);

});


Route::get('/material/autocomplete/resources', function () {
  $query = $this->app->request->input('query');
  return App::make('\Bentleysoft\Http\Controllers\ContentController')->getMaterialAutocomplete($query);

});


$api = app('Dingo\Api\Routing\Router');
$api->version(['v1','v2'], ['namespace' , 'value'=>'App\Http\Controllers'], function ($api) {

  $api->get('v1/category/{id?}',  '\Bentleysoft\Http\Controllers\AcademyController@getCategory' );
  $api->get('v1/course/{id?}',  '\Bentleysoft\Http\Controllers\AcademyController@getCourse' );
  $api->get('v1/subject/{id?}',  '\Bentleysoft\Http\Controllers\AcademyController@getSubject' );
  $api->get('v1/user/{id?}',  '\Bentleysoft\Http\Controllers\AcademyController@getUser' );
  $api->get('v1/reviews/{id?}',  '\Bentleysoft\Http\Controllers\AcademyController@getReviews' );
  $api->get('v1/scores/{id?}',  '\Bentleysoft\Http\Controllers\AcademyController@getScores' );
  $api->post('v1/review/{id?}',  '\Bentleysoft\Http\Controllers\AcademyController@postReview' );

//  $api->get('v1/{resource?}/{id?}',  '\Bentleysoft\Http\Controllers\AcademyController@api' );
  
  $api->get('v2/entry', function($api) {
  
  });
});
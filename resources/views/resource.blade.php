@extends('layouts.jisc_standard')
@section('content')
  @if(false)
  <div class="pattern-library__page-title">
    <div class="inner">
      <h1>{{$resource->fullname}}</h1>
    </div>
  </div>
  @endif

  <main id="main" role="main" class="main">
    <div class="inner l-pull-left featured">
      <div class="l-centre-offset">

        <div class="row">
          <div class="span-8 col">
            <ul class="breadcrumb has-backlink">
              <li>
                <a href="{{url('/')}}">Home</a>
              </li>
              <li class="backlink">
                <a href="{{url('area/find-and-reuse/')}}">Find and Reuse</a>
              </li>
              <li>
                <span>{{$resource->fullname, 'NoName'}}</span>
              </li>
            </ul>
          </div>
        </div>
        @if(Session::has('success'))
        <div class="row">
            <div class="span-8 col timed">
              <article class="box box--padding-large box--success">
                <header class="box--notice__header">
                  Thank you
                </header>
                <div class="box__inner">
                  <p>Your review has been received and posted.</p>
                </div>
                <!--/ box__inner -->
              </article>
            </div>
        </div>
        @endif
        @if(!$overview)
          <div class="row">
            <div class="span-8 col ">
              <div>
                <p>{!! $resource->summary !!}</p>
              </div>
            </div>
            <div class="span-3 col" style="margin-left: 12px;">
              <h2>Ratings {{rand(1,5)}}.{{rand(0,9)}}</h2>
            </div>
          </div>
        @endif
        <div class="row">
          <div class="span-8 col resource">
            @if($overview && count($overview)>0)
              <h2>{{ $overview[0]->name }}</h2>
              <div class="breather">
                <p>{!! $overview[0]->summary !!}</p>
              </div>
            @elseif(count($labels) >0)
              <div>
                <h2>{{$labels[0]->name}}</h2>
                <p>{!! $labels[0]->intro !!}</p>
              </div>
            @endif
          </div>
          <div class="span-3 col user_reviews" style="margin-left: 12px;">
            <h2>{{round($scores->totalScore, 1, PHP_ROUND_HALF_UP)}}</h2>
            <div class="ratty"></div>

          </div>
          <div class="span-3 col user_reviews" style="margin-left: 12px; margin-top: 1em;">
            <h4>Review highlights</h4>
            @include('partials.diskuss')
          </div>
        </div>
        <div class="row">
          <div class="span-8 col reviews">
            <h3 class="orange">User reviews</h3>
            @include('partials.reviews')
          </div>
          <div class="span-3 col">


          </div>
        </div>

      </div>
    </div>
  </main>

<script>
  $('.ratty').raty({ score: <?php echo $scores->totalScore  ?>, readOnly: true });
  $('.rattyLive').raty({ score: 0, readOnly: false });
  $('.ratty1').raty({ score: 1, readOnly: true });
  $('.ratty2').raty({ score: 2, readOnly: true });
  $('.ratty3').raty({ score: 3, readOnly: true });
  $('.ratty4').raty({ score: 4, readOnly: true });
  $('.ratty5').raty({ score: 5, readOnly: true });


</script>

@stop

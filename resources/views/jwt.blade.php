@extends('layouts.jisc_standard')
@section('content')
  <main id="main" role="main" class="main">
    <div class="inner l-pull-left featured">
      <div class="l-centre-offset">

        <div class="row">
          <div class="span-8 col">
            <ul class="breadcrumb has-backlink">
              <li>
                <a href="{{url('/')}}">Home</a>
              </li>
              <li>
                <span>Portal</span>
              </li>
            </ul>
          </div>
        </div>
        @if(Session::has('errors'))
          <div class="row">
            <div class="span-10 col timex">
              <article class="box box--padding-large box--danger">
                <header class="box--notice__header">
                  Something isn't right
                </header>
                <div class="box__inner">
                  <p>Please try again making sure you put in the correct credentials. Alternatively click 'forgot password' to reset it.</p>
                </div>
                <!--/ box__inner -->
              </article>
            </div>
          </div>
        @endif

        <div class="row">
          <div class="span-10 col ">
            <form class="form" method="post" action="{{url('/')}}/jwt">
              <fieldset class="form__fieldset">
                <legend>
                  <span class="legend-text">Login</span>
                </legend>
                @if(\Session::has('errors'))
                  <h3 class="error">{{$errors->getBag('default')->first('error')}}</h3>
                @endif
                <ul class="form-fields">
                  <li class="form-fields__item--text">
                    <label <?php if (\Session::has('errors') &&  $errors->getBag('default')->has('email')) echo 'class="s-error"'?> >
                      <span class="form-fields__label-text">Your username (email)</span>
                      <input type="text" name="email">
                    </label>
                  </li>
                  <li class="form-fields__item--text">
                    <label <?php if (\Session::has('errors') &&  $errors->getBag('default')->has('password')) echo 'class="s-error"'?> >
                      <span class="form-fields__label-text">Your password</span>
                      <input type="password" name="password">
                      {{csrf_field()}}
                      <input type="hidden" name="slug" id="slug" value="{{$slug}}" />
                    </label>
                  </li>

                </ul>
                <!--/ .form-fields -->
                <div class="btn-wrap">
                  <button class="btn btn--3d btn--primary">Login</button>
                </div>
                <div class="btn-wrap">
                  <a href="{{ url('login/reset')}}">Forgot password?</a>
                </div>

              </fieldset>

            </form>
          </div>
        </div>
      </div>
    </div>
  </main>
@stop

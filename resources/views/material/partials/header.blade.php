<header class="header header--focussed" role="banner" data-mobilemenu-focussed="">
    <div class="ksk">
        <div class="sd">
            <a class="header__logo" href="<%url('/')%>/material/">
                <img alt="Jisc logo" src="<%url('/')%>/assets/img/Jisc_LogoAW_RGB.png" width="80">
            </a>
            <div id="nav" class="title-nav" data-dropdown="">
            </div>

        </div>
    </div>
</header>

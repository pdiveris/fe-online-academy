<!DOCTYPE html>
<html lang="en-gb" ng-app="BlankApp">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width,initial-scale=1.0" name="viewport">
  <!-- The above 3 meta tags must come first in the head; any other head content must come after these tags. -->
  <meta content="on" http-equiv="cleartype">
  <title>FE Online Academy</title>
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/angular_material/1.0.0-rc5/angular-material.min.css">
</head>
</head>
<body ng-cloak>

@include('material.partials.header')
@yield('content')
@include('material.partials.footer')

<!-- Angular Material requires Angular.js Libraries -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-animate.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-aria.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-messages.min.js"></script>

<!-- Angular Material Library -->
<script src="https://ajax.googleapis.com/ajax/libs/angular_material/1.0.0-rc5/angular-material.min.js"></script>

<!-- Your application bootstrap  -->
<script type="text/javascript">
  /**
   * You must include the dependency on 'ngMaterial'
   */
  'use strict';
  var app = angular.module('BlankApp', ['ngMaterial']).controller('DemoCtrl', DemoCtrl);

  function DemoCtrl ($timeout, $q, $log, $http) {
    var self = this;
    self.simulateQuery = false;
    self.isDisabled    = false;
    // list of `state` value/display objects
    // self.states        = loadAll();
    self.states        = loadAllFromApi();
    self.querySearch   = querySearch;
    self.selectedItemChange = selectedItemChange;
    self.searchTextChange   = searchTextChange;
    self.newState = newState;

    self.resource = null;

    function newState(state) {
      alert("Sorry! You'll need to create a Constituion for " + state + " first!");
    }
    // ******************************
    // Internal methods
    // ******************************
    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query) {
      return self.states;

      var results = query ? self.states.filter( createFilterFor(query) ) : self.states,
              deferred;

      if (self.simulateQuery) {
        deferred = $q.defer();
        $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
        return deferred.promise;
      } else {
        return results;
      }
    }
    function searchTextChange(text) {
      $log.info('Text changed to ' + text);
    }

    function selectedItemChange(item) {

      $log.info(item);
     // $log.info('Item changed to ' + JSON.stringify(item));

      var request = $http({
        method: "get",
        url: "/material/resource/"+item.url,
        params: {
          action: "get"
        }
      });
      return( request.then( handleGotResource, handleError ) );

    }
    /**
     * Build `states` list of key/value pairs
     */
    function loadAll() {
      var allStates = 'Alabama, Alaska, Arizona, Arkansas, California, Colorado, Connecticut, Delaware,\
              Florida, Georgia, Hawaii, Idaho, Illinois, Indiana, Iowa, Kansas, Kentucky, Louisiana,\
              Maine, Maryland, Massachusetts, Michigan, Minnesota, Mississippi, Missouri, Montana,\
              Nebraska, Nevada, New Hampshire, New Jersey, New Mexico, New York, North Carolina,\
              North Dakota, Ohio, Oklahoma, Oregon, Pennsylvania, Rhode Island, South Carolina,\
              South Dakota, Tennessee, Texas, Utah, Vermont, Virginia, Washington, West Virginia,\
              Wisconsin, Wyoming';
      return allStates.split(/, +/g).map( function (state) {
        return {
          value: state.toLowerCase(),
          display: state
        };
      });
    }

    function loadAllFromApi() {
      var request = $http({
        method: "get",
        url: "/material/autocomplete/resources",
        params: {
          action: "get"
        }
      });
      return( request.then( handleSuccess, handleError ) );
    }
    // I transform the error response, unwrapping the application dta from
    // the API response payload.
    function handleError( response ) {
      // The API response from the server should be returned in a
      // nomralized format. However, if the request was not handled by the
      // server (or what not handles properly - ex. server error), then we
      // may have to normalize it on our end, as best we can.
      if (
              ! angular.isObject( response.data ) ||
              ! response.data.message
      ) {
        return( $q.reject( "An unknown error occurred." ) );
      }
      // Otherwise, use expected error message.
      return( $q.reject( response.data.message ) );
    }

    function handleGotResource( response ) {
      console.log(response.data.overview[0].summary);
      self.resource =  response.data;

      /// return response.data.suggestions;
      // x = response.data;
      // console.log(x);
      // return loadAll();
    }

    function handleSuccess( response ) {
      console.log(response);

      return response.data.suggestions;
      // x = response.data;
      // console.log(x);
      return loadAll();
    }

    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);
      return function filterFn(state) {
        return (state.value.indexOf(lowercaseQuery) === 0);
      };
    }
  }

  app.filter('unsafe', function($sce) {
    return function(val) {
      return $sce.trustAsHtml(val);
    };
  });

</script>

</body>
</html>

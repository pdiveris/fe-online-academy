@extends('material.layouts.jisc_standard')
@section('content')

  <main id="main" role="main">
    <!-- bare for home only -->
    <div class="pattern-library__page-content pattern-library__page-content--nav">
      <div class="inner l-pull-left" style="background-color: #e4e9ec;">
        <div class="l-centre-offset">
        <!-- start main here -->
          <div class="row">
            <section class="region region--3-up" data-equal-height="">

            <h2>LEARN</h2>

            </section>

          </div>
        <!-- end main-->
        </div>
      </div>
    </div>
  </main>
@stop

@extends('material.layouts.jisc_standard')
@section('content')


  @if(true)
  <main id="main" role="main">
    <!-- bare for home only -->
    <div class="pattern-library__page-content pattern-library__page-content--nav">
      <div class="inner l-pull-left" style="background-color: #e4e9ec;">
        <div class="l-centre-offset">
        <!-- start main here -->
          <div class="row">
            <section class="region region--3-up" data-equal-height="">
              @foreach ($areas as $i=>$area)
                <div class="block block-<%$i+1%>">
                  <article class="teaser has-media has-media--two-thirds" style="height: 257px;">
                    <a class="marker" href="<%url('/')%>/material/area/<%str_slug($area->name)%>/"><%$area->name%></a>
                    <div class="teaser__copy">
                      <h2 class="teaser__title" style="height: 54px;">
                        <a href="<%url('/')%>/material/area/<%str_slug($area->name)%>/" ><%$area->name%></a>
                      </h2>
                    </div>
                    <figure class="media">
                      <a href="./about/" class="media__link">
                        <img src="<%url('/')%>/assets/img/<%$area->name%>.jpg" alt="<%$area->name%>" title="<%$area->name%>" >
                      </a>
                    </figure>
                  </article>
                </div>
              @endforeach
              </div>
            </section>
          </div>
        <!-- end main-->
        </div>
      </div>
    </div>
  </main>
  @endif

@stop

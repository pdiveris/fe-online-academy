@extends('layouts.jisc_standard')
@section('content')

  <div class="pattern-library__page-title">
    <div class="inner">
      <h1>Login</h1>
    </div>
  </div>
  <main id="main" role="main">
    <!-- bare for home only -->
    <div class="pattern-library__page-content pattern-library__page-content--nav">
      <div class="inner l-pull-left">
        <div class="l-centre-offset resource">
          <div class="row">
            <div class="span-12 col ">

              <form class="form" method="post" action="{{url('/')}}/login/reset">
                <fieldset class="form__fieldset">
                  <legend>
                    <span class="legend-text">Reset your password</span>
                  </legend>

                  @if(\Session::has('success'))
                    <h3 class="success">{{\Session::get('success')}}</h3>
                  @endif
                  @if(\Session::has('errors'))
                    <h3 class="error">{{$errors->getBag('default')->first('error')}}</h3>
                  @endif
                  <ul class="form-fields">
                    <li class="form-fields__item--text">
                      <label <?php if (\Session::has('errors') &&  $errors->getBag('default')->has('email')) echo 'class="s-error"'?> >
                        <span class="form-fields__label-text">Your username (email)</span>
                        <input type="text" name="email">
                        {{csrf_field()}}
                      </label>
                    </li>

                  </ul>
                  <!--/ .form-fields -->
                  <div class="btn-wrap">
                    <button class="btn btn--3d btn--primary">Reset</button>
                  </div>

                </fieldset>

              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>

@stop

@extends('layouts.jisc_standard')
@section('content')
  <main id="main" role="main" class="main">
    <div class="inner l-pull-left featured">
      <div class="l-centre-offset">

        <div class="row">
          <div class="span-8 col">
            <ul class="breadcrumb has-backlink">
              <li>
                <a href="{{url('/')}}">Home</a>
              </li>
              <li>
                <span>Join</span>
              </li>
            </ul>
          </div>
        </div>
        @if(Session::has('errors'))
          <div class="row">
            <div class="span-10 col timex">
              <article class="box box--padding-large box--danger">
                <header class="box--notice__header">
                  Something isn't right
                </header>
                <div class="box__inner">
                  <p>
                    Please see below and correct the form
                  </p>
                </div>
                <!--/ box__inner -->
              </article>
            </div>
          </div>
        @endif

        <div class="row">
          <div class="span-10 col ">

            <form class="form" method="post" action="{{url('/')}}/join">
              <fieldset class="form__fieldset">
                <legend>
                  <span class="legend-text">Join</span>
                </legend>

                <ul class="form-fields">
                  <li class="form-fields__item--text">
                    <label <?php if (\Session::has('errors') &&  $errors->getBag('default')->has('firstname')) echo 'class="s-error"'?> >
                      <span class="form-fields__label-text">Name</span>
                      <input type="text" value="{{\Input::old('firstname')}}" name="firstname">
                    </label>
                  </li>
                  <li class="form-fields__item--text">
                    <label <?php if (\Session::has('errors') &&  $errors->getBag('default')->has('lastname')) echo 'class="s-error"'?> >
                      <span class="form-fields__label-text">Surname</span>
                      <input value="{{\Input::old('lastname')}}" type="text" name="lastname">
                    </label>
                  </li>

                  <li class="form-fields__item-select">
                    <label>
                      <span  class="form-fields__label-text">Institution</span>
                        <select data-dynamite-selected="true">
                          <option value="0">Please select</option>
                          <option value="1">Burnley College</option>
                          <option value="2">Manchester Academy</option>
                          <option value="3">Birmingham Beauty Academy</option>
                          <option value="9">This is a ridiculously long option to test the width</option>
                        </select>
                    </label>
                  </li>

                  <li class="form-fields__item--text">
                    <label <?php if (\Session::has('errors') &&  $errors->getBag('default')->has('email')) echo 'class="s-error"'?> >
                      <span class="form-fields__label-text">Your email</span>
                      <input value="{{\Input::old('email')}}"type="text" name="email">
                      @if ($errors->getMessageBag()->has('email'))
                        <span class="error-message">{{$errors->getMessageBag()->first('email')}}</span>
                      @endif

                    </label>
                  </li>

                  <li class="form-fields__item--text">
                    <label <?php if (\Session::has('errors') &&  $errors->getBag('default')->has('password')) echo 'class="s-error"'?> >
                      <span class="form-fields__label-text">Choose a password</span>
                      <input type="password" name="password">
                      @if ($errors->getMessageBag()->has('password'))
                        <span class="error-message">{{$errors->getMessageBag()->first('password')}}</span>
                      @endif

                    </label>
                  </li>

                  <li class="form-fields__item--text">
                    <label <?php if (\Session::has('errors') &&  $errors->getBag('default')->has('password_confirmation')) echo 'class="s-error"'?> >
                      <span class="form-fields__label-text">Confirm password</span>
                      <input type="password" name="password_confirmation">

                      @if ($errors->getMessageBag()->has('password_confirmation'))
                        <span class="error-message">{{$errors->getMessageBag()->first('password_confirmation')}}</span>
                      @endif

                      {{csrf_field()}}
                    </label>
                  </li>
                </ul>

                <div class="btn-wrap">
                  <button class="btn btn--3d btn--primary">Send</button>
                </div>
              </fieldset>
            </form>
          </div>
        </div>

      </div>
    </div>
  </main>
  <script src="{{url('/')}}/assets/ux/js/ux.jisc-1.2.0.script-foot.js"></script>

@stop

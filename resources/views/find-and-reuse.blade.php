@extends('layouts.jisc_standard')
@section('content')


  <main id="main" role="main" class="main">
    <div class="inner l-pull-left featured">
      <div class="l-centre-offset">
        @if(Session::has('success'))
        <div class="row">
            <div class="span-8 col timed">
              <article class="box box--padding-large box--success">
                <header class="box--notice__header">
                  Thank you
                </header>
                <div class="box__inner">
                  <p>Your review has been received and posted.</p>
                </div>
                <!--/ box__inner -->
              </article>
            </div>
        </div>
        @endif
        <div class="row">
          <br/>
          <br/>
        </div>
        <div class="row">
          <div class="span-8">
          <strong style="display: inline;">Search</strong>
          <input class="input--large"
                 id="autocomplete-ajax"
                 style="width: 80%; important; z-index: 2;">

          <div id="xselection-ajax" >

          </div>
        </div>
          <!--div class="span-8 col resource">
          </div-->
          <!--div class="span-3 col user_reviews" style="margin-left: 12px;">
          </div-->
          <div class="row" style="margin-top: 2.2em;">
            <h3>
              Teaching and learning resources that you can use in your own teaching along with guidance on how to make the most of them
            </h3>
          </div>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
        </div>
      </div>
    </div>
  </main>
@stop

@extends('layouts.jisc_standard')
@section('content')
  <main id="main" role="main" class="main">
    <div class="inner l-pull-left featured">
      <div class="l-centre-offset">

        <div class="row">
          <div class="span-8 col">
            <ul class="breadcrumb has-backlink">
              <li>
                <a href="{{url('/')}}">Home</a>
              </li>
              <li>
                <span>Build</span>
              </li>
            </ul>
          </div>
        </div>

        <div class="row">
          <h2>Build</h2>
        </div>
        <div class="row">
          @if(\Input::get('lego','')<>'')
            <img src="{{url('/assets/img')}}/mindstorms.jpg" alt="Build!" title="Build!" />
          @endif
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
        </div>

      </div>
    </div>
  </main>
@stop

<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <style type="text/css">
    .header__top {
        background-color: #2B3840;
        border-bottom: 5px solid #a92173;
        height: 51px;
        height: 5.1rem;
        overflow: visible;
    }

    .header__top a {
        display: inline;
        text-decoration: none;
        color: #ffffff;

    }
    .header__top a:visited {
        display: inline;
        text-decoration: none;
        color: #ffffff;
    }
    header__top img {
        float: left;
    }
    #left {
        width: 30%;
        float: left;
    }
    #right {
        display: inline;
        float: left;
        width: 50%;
    }
    .header__top h3 {
        color: #ffffff;
    }
    </style>
</head>
<body>
<div class="header__top" data-dynamite-selected="true">
    <div id="left">
        <a class="header__logo" href="{{url('/')}}">
            <img
                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD0AAAAiCAMAAADbPCGVAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAwBQTFRF86Ry6mUS8Zpm504A6FkE//7986Z5/vj2/fTv8p9t/Org7G4h8qZ2+dS+86Fv98iq9r6X8Y9W7Hcr7HQm7HIl9LGK6FIA62ka/vr36mcZ/fDp8p9r/e7l6mEN8JJc++DR8JFW8JBV8I9T+tvJ+dfC86d3+M+15k0A624i86l/6mcU62UN6V4H74pN6FAA7odI7oE+7X057Xs37Xgt6mMQ6l0F51EA6WAA6mAA6V8A////6l8A6V4A5mQK52QL5mMK6FYA6V0A6FgA6FUA6FcA6FkA6VwE+dXA6VwA6VsA52ML6VoA8JJa/vz66l4A52QK7oJB6VkA51MA/fLr/fHq624g//z66FQA52MK++LT+9/P6FoA6FsA7oNB6GML/Orf6VgA51IA//38508A//39+M2y6WEA620f6V4E6VcA/vr4/vXw624h6GQL+9zK7oI76mMM++TW74dI+Miq7HEj9bSJ9sKi6l8H6VsC/vf06mIO+Mqs7HMn6V0E8I1O98eo+tzL+da+/vz85mcV8qBs9LON6l4L74hI+93M+97M6GIH8I9R98ao9r+d7n837oZK7Xs0618A62oc/Ofc74xL63Aj6VkB63Ek86l9/OXY/vz57n456mEA/vj06FkC745S//z77oFA9reT74lH620g62wj8ZZg++XY741P9bmU86t76VwB5mQM/Ojc75FX6FUC+dO76VwD+tzJ7oNE7YJB7odD9ryZ+tjC9LCH8qBq++PV6VUA6WAJ6WAK6WAB6mAM+tzK+t3L+93L8JVf9K2A9K6G+NG36V8B7oNA7Xgv8JFa7HUp7HUq9baP8pxn/fPu/Ofb/e/n7X037X417n81//r4//v56mQP62YP98Sl/vLr7X466VwH7n4686F07G8j8qJz6V0F8ZNb98Wo/Ovi+Muv74tL6mYX+dvI/fHr86V2+djF+tnF+tnG6V8J6l4I/fTw/fLs/fLt/fPt6WEF6mEH+t7N52MJ+97N86x+62kS62wY6mgV98ap/Ore+dXBuLeWDAAABNVJREFUeNpcVQdgFFUQXYiAMQohlBgBCYIgISA1+//s3t22C3ecXsiZmCokSI9Ue8XesfeGvffesfeu2BVF7L33MuUvqHu3d/v//pl58/6b+VZS27bGy6ZL48exbaVtZSulcEbxSD6KbkVjW8sSK8nvNS/VsR090pcn5a1i9/gOAzn4y16saQ7PO7TM5kDihh6VcmzjggASiH8Bwa81TTm0jLBLSA6dizLshSHIL3nXymRIWHBoJZ04OQ6dS6fTrso9/dEfubm0iuYRqLg1HmzDCSHHqI4ExKWPP/PWuRNej678Flq3a49dkqkjFDqGAUOhNcPZkK+y3dotZs1aM77wOQBcHRKbMSpBzFHYWisHb0YuoDBBt3YEwKyLakaidW+PImpJVTOHDJlyxh/eM+Sc/Ulmbu1UgB2nFyYd3PXGZNdRspsxfUK2UMeUa8wbs3EM22J9a3126enX+ESr4/qu5Mjb7/q+QygELHFOQlMmJxM78As1Iekq8KYMqQ7CNqHV94Kv7qsPfcEiyHVModqI/OXS0j2rXB2ML/2ydXif4v5ZTFp1vNb9wwd/GVlcFRlNKWUtMmSyGtl65+lFDwP0WtsenLUG+Pp+8zalvAnHyKhledrIdoNShUSx3mXXHQB22q0wFpc2tLasPxTuDO1g4qc4/Kt1JsDs7QNTCqI1JWI0ebN1Q+PA4wE+uezrIeUn9OyXdVc/APBm94rqisqzG8pysv2U90aBx3mz9bx7DgH4c1RT0BaEvoq2AXi177is397UuCQXl7Il9a1EUjHydYi8ZODuAOfsdVT5Ag9ZDlcCjEi7JNVMPUXi8rCSRoRUCzovrHHeJYVVTNLM/dctTet9vgAY0KREqIo5xpu7g4T/H2sl7W2fdQnLD70U3tsC0DOca9rQBs6TKi5knMtP+ZvyLmLWMjoqO2JoLzLfrPyCowEebWLEolxSnCZrEqJ26T/T7TqAhRPHkXVJ1rbT4eoni4ei+V2j+gA8Fv0HNnWmJPc558cxYdZ1iypx5eDfCrzfmUzkKzfX+fELAM+OGgRw/kkFZZ/npzNaugPGXkTeVHT4T8MqbjryxPVo/UGH9xQhj/pX7heGYefkKwAeqfl9K4DhazubCm/3vmp+nnXN9Y05+1WDkdzFLT3QeMt36j2KfekNK+DulaPfHb0YoMcTHd5z+O7Fi98bcDPA8sD0CeE8f6C1UOiFCweFtrc1wKbzNukD8XX7GU4++DUezTnFV9TOtCgVdePdMud5fLHv1LGBtqO+33Sd2pje+/Ibv8O52cu2rc7YKh+suoM2YJk1P2v6qrZm2NJwgnT/S64/raw2osodc0C/PX5Qrpc987bK417phmKjHhK9P+mgYUtwpFmdmDFam4bv1nueh92YnvO5dJZqz81F3oJ0Rpn69+sjL8hwl5GWTDqXli0HQXwc0KYoHR8lpv5F1NIVWbLImlbizSif26e2TSs1qjJHkPReKWjFVeI42pwjDEfFSrTNycZOqX8LBifu4LSY+xq+MfFFgNw2Kag2XVibc89Y8eFD4K2fOTfcPvxwq7dPRm9o6TjEFE1jPGVOKnzAd66DdU4erMOaE3WJRF1zIpGsS6TwMZlKpFIJunlMo+ZUqg6nE4nmFI0TzTiXuvZYff8/AgwA27oisvJNgvIAAAAASUVORK5CYII="
                nosend="1" alt="Jisc" title="Jisc" border="0" height="35" width="60"
            />
        </a>
    </div>
    <div id="right">
        <a href="{{url('/')}}"><h3>FE and Skills Online Academy</h3></a>
    </div>
</div>
<p>
Hi {{$data['user']->firstname}} {{$data['user']->last_name}},<br/><br/>
</p>
<p>
We have received a reuqest to reset your password.<br />
If that was send by you then please the code below to reset your password.<br/><br/>
Please visit <a href="{{url('login/change')}}">{{url('login/change')}}</a> and use this code  {{$data['code']}} to reset your password.
</p>
</body>
</html>
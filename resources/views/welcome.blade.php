@extends('layouts.jisc_standard')
@section('content')
    <main id="main" role="main" class="main">
      <div class="inner l-pull-left featured">
        <div class="l-centre-offset">
          <div class="row" style="padding-top: 5em;">
            &nbsp;
          </div>
          <div class="row">
            <article class="teaser">
              <div class="teaser__copy">
                <h2 class="teaser__title">
                  Welcome to the Jisc FE and Skills online continuing professional development service, where you’ll find a wealth of resources to improve your digital capabilities.
                </h2>
                <p>Choose a pathway below to get started!</p>
              </div>
            </article>

          </div>
          <section class="region region--3-up" data-equal-height="">
            <div class="row">
              @foreach ($areas as $i=>$area)
                <div class="block block-{{$i+1}}">
                  <article class="teaser has-media has-media--two-thirds" style="height: 257px;">
                    <a class="marker" href="{{url('/')}}/area/{{str_slug($area->name)}}/">{{$area->name}}</a>
                    <div class="teaser__copy">
                      <h2 class="teaser__title" style="height: 54px;">
                        <a href="{{url('/')}}/area/{{str_slug($area->name)}}/" >{{$area->name}}</a>
                      </h2>
                    </div>
                    <figure class="media">
                      <a href="./about/" class="media__link">
                        <img src="{{url('/')}}/assets/img/{{$area->name}}.jpg" alt="{{$area->name}}" title="{{$area->name}}" >
                      </a>
                    </figure>
                  </article>
                </div>
            @endforeach
            </div>
          </section>

        </div>
        <!-- end main-->
      </div>

  </main>
@stop

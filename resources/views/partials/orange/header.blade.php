<header class="header header--focussed" role="banner" data-mobilemenu-focussed="">
    <div class="header__top">
        <div class="inner">
            <a class="header__logo" href="{{url('/')}}">
                <img alt="Jisc logo" src="{{url('/')}}/assets/img/Jisc_LogoAW_RGB.png" width="43" height="25">
            </a>
            <div id="nav" class="title-nav" data-dropdown="">
                <div class="has-popup header__nav__item" data-dropdown-item="">
                    <h1 class="header__title header__title--long">
                        <a href="{{url('/')}}">
                            FE Online Academy
                        </a>
                    </h1>
                    <div class="submenu">
                        <ul>
                            <li><a href="#">About</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <nav class="header__nav header__nav--primary" role="navigation" data-dropdown="">
                <ul>
                    <li class="header__nav__item header__nav__item--button  ">
                        <a href="#">Portal</a>
                    </li>
                    <li class="header__nav__item  has-popup " data-dropdown-item="">
                        <a href="#">Support</a>
                        <div class="submenu">
                            <ul>
                                <li><a href="#">JiscExchange</a></li>
                                <li><a href="#">Frequently asked questions</a></li>
                                <li><a href="#">Contact Support</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</header>

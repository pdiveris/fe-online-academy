<footer role="contentinfo">
    <!--
    .light here denotes depth of bg tint
   -->
    <div class="inner l-pull-left light">
        <div class="l-centre-offset row">
            <!--
            Based on the way these elements behave when folded down,
            this is a standard row/col layout
           -->
            <div class="col span-6">
                <div class="linklist l-gutter--right">
                    <div class="linklist__title">
                        Contact the FE Online Academy helpdesk
                    </div>
                    <div class="row">
                        <div class="col span-6">
                            <div class="l-gutter--right">
                                <ul>
                                    <li class="linklist__item">
                                        Hours
                                        <p>Monday to Friday 08:00 to 21:00
                                            <br>Saturday 08:00 to 17:00</p>
                                    </li>
                                    <li class="linklist__item">
                                        Telephone
                                        <p>(+44) 0131 650 4933</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col span-6">
                            <div class="l-gutter--right">
                                <ul class="item-set-2">
                                    <li class="linklist__item">
                                        Email
                                        <p><a href="mailto:XYZ@jisc.ac.uk">XYZ@jisc.ac.uk</a></p>
                                    </li>
                                    <li class="linklist__item">
                                        <p>We are keen to improve our services, you can help by sparing
                                            2 minutes to <a href=''>tell us what you think</a> or
                                            <a href=''>view the feedback</a></p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ .linklist -->
            </div>
            <!--/ .col.span-3 -->
            <div class="col span-6">
                <div class="linklist linklist--2col l-gutter--right">
                    <div class="linklist__title">
                        Useful links
                    </div>
                    <ul>
                        <li class="linklist__item">
                            <a href="#">Alpha</a>
                        </li>
                        <li class="linklist__item">
                            <a href="#">Beta</a>
                        </li>
                        <li class="linklist__item">
                            <a href="#">Gamma</a>
                        </li>

                    </ul>
                </div>
                <!--/ .linklist -->
            </div>
            <!--/ .col.span-3 -->
        </div>
        <!--/ /-centre-offset.row -->
    </div>
    <!--/ .inner.l-pull-left -->
    <!--
    .medium here denotes depth of bg tint
   -->
    <div class="inner l-pull-left medium bottom-section">
        <div class="l-centre-offset row">
            <div class="col span-6">
                <div class="l-gutter--right">
                    <div class="divisional-info">
                        <div class="divisional-info__side">
                            <a href="https://jisc.ac.uk">
                                <img alt="Jisc logo" src="{{ url('/')}}/assets/img/jisc-logo.png" width="158"
                                     height="93">
                            </a>
                        </div>
                        <div class="divisional-info__body">
                            <p>Our suite of network and IT services enable close-knit communities
                                of academics, researchers and students to connect and collaborate.</p>
                            <p><a href="#">Find out more at jisc.ac.uk</a></p>
                        </div>
                    </div>
                    <!--/ .divisional-info -->
                </div>
                <!--/ .l-gutter--right -->
            </div>
            <!--/ .col.span-6 -->
            <div class="col span-6">
                <div class="linklist linklist--2col">
                    <div class="linklist__title v-pad-small--mobile">
                        <h3>Network &amp; IT services</h3>
                    </div>
                    <ul>
                        <li class="linklist__item">
                            <a href="#">Security</a>
                        </li>
                        <li class="linklist__item">
                            <a href="#">Cloud</a>
                        </li>
                        <li class="linklist__item">
                            <a href="#">Connectivity</a>
                        </li>
                        <li class="linklist__item">
                            <a href="#">Email</a>
                        </li>
                        <li class="linklist__item">
                            <a href="#">Access and identity <span class="linklist__item__break">management</span></a>
                        </li>
                        <li class="linklist__item">
                            <a href="#">Internet and IP <span class="linklist__item__break">services</span></a>
                        </li>
                        <li class="linklist__item">
                            <a href="#">Procurement</a>
                        </li>
                        <li class="linklist__item">
                            <a href="#">Telecoms</a>
                        </li>
                        <li class="linklist__item">
                            <a href="#">Videoconferencing</a>
                        </li>
                    </ul>
                </div>
                <!--/ .linklist -->
            </div>
            <!--/ .col.span-6 -->
        </div>
        <!--/ .l-centre-offset.row -->
    </div>
    <!--/ .inner.l-pull-left.medium -->
    <!--
    .heavy here denotes depth of bg tint
   -->
    <div class="inner l-pull-left heavy">
        <div class="l-centre-offset row cc-wrap">
            <div class="cc duo">
                <div class="duo__side">
                    <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/2.0/uk/">
                        <img src="{{url('/')}}/assets/img/cc_BY-NC-SA.png" alt="License" width="140" height="49">
                    </a>
                </div>
                <div class="duo__body">
                    <!--
                @CHANGE
                            Add the following print version of the copyright notice. Should always display current year
                            -->
                    <span class="print-only">&copy;{{date('Y')}} Jisc.</span> This work is licensed under the
                    <a class="cc__link"
                       rel="license" href="http://creativecommons.org/licenses/by-nc-nd/2.0/uk/">CC BY-NC-SA 4.0</a>
                </div>
            </div>
            <!--/ .cc.duo -->
        </div>
        <!--/ .l-centre-offset.row -->
    </div>
    <!--/ .inner.l-pull-left.heavy -->
</footer>
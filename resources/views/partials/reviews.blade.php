<!-------------------------------------  diskuss ---------------------------------------->
<ul class="raty" xmlns="http://www.w3.org/1999/html">
    @foreach($reviews as $review)
        <li>
            <span class="ratty{{$review->score}}"></span>
            by <strong>{{$review->user->firstname}} {{$review->user->lastname}}</strong>
            <h5><em>{{$review->title}}</em></h5>
            <div>{{$review->comments}}</div>
        </li>
    @endforeach
</ul>

@if (null!==$user)
    <a name="review"></a>
    <div id="form" style="margin-top: 21px;">
        <h4>Your review</h4>
        <form class="form" id="review_form" action="{{url('/review')}}" method="post">
            {{csrf_field()}}

                <fieldset class="form__fieldset">
                    <legend>
                        <span class="legend-text">Leave your comment</span>
                    </legend>
                    <ul class="form-fields">
                        <li class="form-fields__item--text">
                            <label>
                                <span class="form-fields__label-text">Rating</span>
                                <div class="rattyLive"></div>

                            </label>
                        </li>
                        <li class="form-fields__item--text">
                            <label class="{{\Bentleysoft\Http\Controllers\ContentController::getClassForLabel('title', $errors)}}">
                                <span class="form-fields__label-text">Summary</span>
                                @if ($errors->getMessageBag()->has('title'))
                                    <span class="error-message">{{$errors->getMessageBag()->first('title')}}</span>
                                @endif
                                <input type="text" id="feoa_user_comment_title" name="title" />
                            </label>
                        </li>
                        <li class="form-fields__item--textarea">
                            <label class="{{\Bentleysoft\Http\Controllers\ContentController::getClassForLabel('comments', $errors)}}">
                                <span class="form-fields__label-text">Your comment</span>
                                <textarea id="feoa_user_comment_comment" name="comments" /></textarea>
                                @if ($errors->getMessageBag()->has('comments'))
                                    <span class="error-message">{{$errors->getMessageBag()->first('comments')}}</span>
                                @endif
                            </label>
                        </li>
                    </ul>
                    <!--/ .form-fields -->
                    <div class="btn-wrap">
                        <input type="submit" class="btn btn--3d btn--primary">Leave comment</input>
                    </div>
                </fieldset>

            <input type="hidden" name="resource_id" value="{{$resource->id}}"/>
            <input type="hidden" name="resource_shortname" value="{{$resource->shortname}}"/>
        </form>
    </div>

@endif
@if(null!=$user)
    <a class="feoa_logout_link" href="{{url('/')}}/logout">Logout {{$user->firstname}} {{$user->lastname}}</a>
@else
    <a href="{{url('/')}}/login?resource={{$resource->shortname}}">Login to review</a>
@endif

@if (Session::has('errors'))
    <script>
    $("html, body").animate({ scrollTop: $(document).height()-($(window).height()*0.6) });
    </script>
@endif

<!-------------------------------------  diskuss ---------------------------------------->
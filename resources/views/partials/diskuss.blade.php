<!-------------------------------------  diskuss ---------------------------------------->
<ul>
    @foreach($reviews as $review)
        <li>
            <strong>{{$review->title}}</strong>, {{$review->user->firstname}} {{$review->user->lastname}}
        </li>
    @endforeach
</ul>
@if (false)
    <div style="text-align: right;">
        <i class="fa fa-comment"></i>&nbsp;
    </div>
    <div style="border: 1px solid #cccccc"></div>
    <div id="form" style="margin-top: 21px;">
        <form id="review_form" action="{{url('/review')}}" method="post">
            {{csrf_field()}}
            <input type="text" id="feoa_user_comment"
                   name="feoa_user_comment" <?php if (null == $user) echo 'readonly="readonly"' ?> />
            <input type="hidden" name="resource_id" value="{{$resource->id}}"/>
            @if(null!==$user)
                <a class="btn" href="#" onclick="return $('#review_form').submit();">
                    <i class="fa fa-send fa-lg"></i>
                </a>
            @endif
        </form>
        @if(null!=$user)
            <a class="feoa_logout_link" href="{{url('/')}}/logout">Logout {{$user->firstname}} {{$user->lastname}}</a>
        @else
            <a href="{{url('/')}}/login">Login to review</a>
        @endif
    </div>
@endif
<!-------------------------------------  diskuss ---------------------------------------->
@extends('layouts.jisc_standard')
@section('content')
<style type="text/css">
  .ul-options {
    padding:12px 0px 30px 0px;
    border-bottom: #efefef solid 1px;
    margin-bottom: 20px;
  }
  .ul-options li{
    padding:5px 10px 5px 0px;
  }

  .ul-options .option {
    margin-left:30px;

  }
  .ul-options input {
    float:left;
    margin-top:6px;
  }


  .quiz-finish {
    text-align: left;
    margin-bottom: 20px
  }
  .quiz-box {
    padding: 20px 0;
  }
  .quiz-resource-ul {
    list-style-type: none;
    list-style: none;
  }
  .ul-sections>li>span {
    display: block;
    font-size: 30px;
    font-weight: bold;
    margin-bottom: 10px;
  }
  .section-name {
    padding: 15px 15px 15px 15px;

    background-color: #efefef;
  }
  .ul-questions>li>span {
    display: block;
    font-weight: bold;
    margin-top: 1.4em;
    font-size: 20px;
  }

</style>
  <main id="main" role="main" class="main">
    <div class="inner l-pull-left featured">
      <div class="l-centre-offset">

        <div class="row">
          <div class="span-8 col">
            <ul class="breadcrumb has-backlink">
              <li>
                <a href="{{url('/')}}">Home</a>
              </li>
              <li>
                <span>Learn</span>
              </li>
            </ul>
          </div>
        </div>

        <div class="row" style="margin-top: 1.1em;">
        <ul class="ul-sections">
          <?php
          foreach (\Bentleysoft\Http\Controllers\ContentController::getDiagnosticToolQueations()->sections as $i=>$section) {
          ?>
            <li data-ng-repeat="section in sections" class="ng-scope">
              <span class="section-name" data-ng-bind="section.name">{{$section->name}}</span>
              <ul class="ul-questions">
                <!-- ngRepeat: question in section.questions -->
                <li data-ng-repeat="question in section.questions" class="ng-scope">

                  @foreach($section->questions as $j=>$question)
                    <span data-ng-bind="question.name" class="ng-binding">{{$question->name}}</span>
                    <ul class="ul-options">
                      @foreach($question->options as $k=>$option)
                        <li data-ng-repeat="option in question.options" class="ng-scope">
                          <label>
                            <input type="radio" value="{{$option->_id}}"  name="{{$option->question}}" id="option-{{$option->_id}}" >
                            <div class="option ng-binding">{{$option->name}}</div>
                          </label>
                        </li>
                      @endforeach
                    </ul>
                  @endforeach




                </li><!-- end ngRepeat: question in section.questions -->
              </ul>
            </li><!-- end ngRepeat: section in sections --><li data-ng-repeat="section in sections" class="ng-scope">

          <?php
          }
          ?>
        </ul>

          <div class="quiz-finish">
            <div class="thank-you">Thank-you! Enter your email address and click the button below to submit your answers and get your results.
              <div class="thank-you-footer">Please note, this project is still in the exploration phase, and we will undoubtedly use your answers to learn and find ways to improve things. By submitting your answers you agree that you're happy for us to do that.
              </div></div>

            <input size="35" type="text" ng-model="reference" name="reference" placeholder="Email address" class="ng-pristine ng-untouched ng-valid">
            <button class="btn btn--3d btn--primary" ng-click="complete()">
              Get your results
            </button>

          </div>
        </div>

      </div>
    </div>
  </main>
@stop

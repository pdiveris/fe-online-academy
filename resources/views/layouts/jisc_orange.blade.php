<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="lt-ie9 lt-ie8 ie7"><![endif]-->
<!--[if IE 8]><html class="lt-ie9 ie8"><![endif]-->
<!--[if IE 9]><html class="ie9"><![endif]-->
<!--[if gt IE 9]><!-->
<html lang="en-gb">
<!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width,initial-scale=1.0" name="viewport">
  <!-- The above 3 meta tags must come first in the head; any other head content must come after these tags. -->
  <meta content="on" http-equiv="cleartype">
  <title>FE Online Academy <?php if(isset($meta_title)) echo ' :: '. $meta_title; ?></title>
  <link rel="shortcut icon" href="https://ux.jisc.ac.uk/0.2.0/favicon.ico">
  <!--[if IE]><![endif]-->
  <!--[if lte IE 8]>
  <link rel="stylesheet" href="https://ux.jisc.ac.uk/0.2.0/css/ux.jisc-0.2.0.style-oldie.min.css" media="all">
  <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
  <![endif]-->
  <!--[if gt IE 8]><!-->
  <link rel="stylesheet" href="{{url('/')}}/assets/ux/css/ux.jisc-0.2.0.style.min.css" media="all">
  <!--<![endif]-->
  <script src="{{url('/')}}/assets/js/ux.jisc-0.2.0.script-head.min.js"></script>

  <!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script-->
  <script src="{{url('/')}}/assets/jquery/jquery-1.8.2.min.js"></script>
  <script src="{{url('/')}}/assets/jquery/autocomplete/jquery.autocomplete.js"></script>
  <script src="{{url('/')}}/assets/jquery/autocomplete/countries.js"></script>
  <script src="{{url('/')}}/assets/jquery/autocomplete/jquery.mockjax.js"></script>
  <script src="{{url('/')}}/assets/jquery/raty/jquery.raty.js"></script>
  <link rel="stylesheet" href="{{url('/')}}/assets/jquery/raty/jquery.raty.css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />

  <style type="text/css">
    .autocomplete-suggestions {
      border: 1px solid #999;
      background: #FFF; cursor:
      default; overflow: auto;
      -webkit-box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64);
      -moz-box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64);
      box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64);
    }
    .autocomplete-suggestion {
      padding: 2px 5px;
      white-space: nowrap;
      overflow: hidden;
    }
    .autocomplete-selected {
      background: #F0F0F0;
    }
    .autocomplete-suggestions strong {
      font-weight: normal; color: #3399FF;
    }
    .autocomplete-group strong { display: block; font-style: italic; color: #000000 }

    .resource tr {
      background-color: #ffffff !important;
    }
    .resource td:nth-of-type(2) {
      text-align: center !important;
    }

    div.user_reviews ul li {
      list-style: none !important;
      list-style-type: none !important;
      background-image: none !important;
    }

    .user_reviews ul {
      font-size: 0.8em;
    }

    .user_reviews input {
      width: 79%;
    }

    .error {
      color: #cc0000;
    }

    .success {
      color: #339933;
    }

    .feoa_logout_link {
      display: block;
      font-size: 0.8em;
      margin-top: 7px;
    }
    .breather {
      line-height: 1.4em;
    }
    .nomargin {
      margin: 0px !important;
      padding: 0px !important;
    }
    .resource {
      padding-bottom: 12px;

    }

    .resource strong {
      line-height: 2em;
    }

    .orange {
      font-weight: 700;
      margin-top: 11px;
      color: darkorange;
    }

    .raty {
      margin-top: -6px;
    }
    .raty li {
      padding-bottom: 13px;;
    }

    .rattyLive {
      margin-top: 5px;
    }
  </style>
</head>
<body>
@include('partials.header')
  @yield('content')
@include('partials.footer')

<!--script src="{{url('/')}}/assets/ux/js/ux.jisc-0.2.0.script-foot.min.js"></script-->
<!-- Insert Google Analytics here either using either Google Tag Manager, or by adding the tracking code directly onto every page. We include GA in this location on the page for performance reasons. -->
<script>
  $(document).ready(function(){
    $(".timed").delay(3200).fadeOut(300);
    $(".autoselect").change(function() {
          $('.search').submit();
        }
    );
  });
</script>
<script src="{{url('/')}}/assets/jquery/autocomplete/autodoodah.js"></script>
</body>
</html>
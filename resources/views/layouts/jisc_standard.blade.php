<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="lt-ie9 lt-ie8 ie7"><![endif]-->
<!--[if IE 8]><html class="lt-ie9 ie8"><![endif]-->
<!--[if IE 9]><html class="ie9"><![endif]-->
<!--[if gt IE 9]><!-->
<html lang="en-gb">
<!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width,initial-scale=1.0" name="viewport">
  <!-- The above 3 meta tags must come first in the head; any other head content must come after these tags. -->
  <meta content="on" http-equiv="cleartype">
  <title>FE Online Academy <?php if(isset($meta_title)) echo ' :: '. $meta_title; ?></title>
  <link rel="shortcut icon" href="https://ux.jisc.ac.uk/0.2.0/favicon.ico">
  <!--[if IE]><![endif]-->
  <!--[if lte IE 8]>
  <!--link rel="stylesheet" href="https://ux.jisc.ac.uk/0.2.0/css/ux.jisc-0.2.0.style-oldie.min.css" media="all"--
  >
  <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
  <![endif]-->
  <!--[if gt IE 8]><!-->

  <!--link rel="stylesheet" href="{{url('/')}}/assets/ux/css/ux.jisc-0.2.0.style.min.css" media="all"-->
  <link rel="stylesheet" href="{{url('/')}}/assets/ux/css/ux.jisc-1.2.0.style-oldie.min.css" media="all">
  <!--<![endif]-->
  <script src="{{url('/')}}/assets/ux/js/ux.jisc-1.2.0.script-head.min.js"></script>

  <!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script-->
  <script src="{{url('/')}}/assets/jquery/jquery-1.8.2.min.js"></script>
  <script src="{{url('/')}}/assets/jquery/autocomplete/jquery.autocomplete.js"></script>
  <script src="{{url('/')}}/assets/jquery/autocomplete/countries.js"></script>
  <script src="{{url('/')}}/assets/jquery/autocomplete/jquery.mockjax.js"></script>
  <script src="{{url('/')}}/assets/jquery/raty/jquery.raty.js"></script>
  <link rel="stylesheet" href="{{url('/')}}/assets/jquery/raty/jquery.raty.css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />

  <style type="text/css">
    .autocomplete-suggestions {
      border: 1px solid #999;
      background: #FFF; cursor:
      default; overflow: auto;
      -webkit-box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64);
      -moz-box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64);
      box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64);
    }
    .autocomplete-suggestion {
      padding: 2px 5px;
      white-space: nowrap;
      overflow: hidden;
    }
    .autocomplete-selected {
      background: #F0F0F0;
    }
    .autocomplete-suggestions strong {
      font-weight: normal; color: #3399FF;
    }
    .autocomplete-group strong { display: block; font-style: italic; color: #000000 }

    .resource tr {
      background-color: #ffffff !important;
    }
    .resource td:nth-of-type(2) {
      text-align: center !important;
    }

    div.user_reviews ul li {
      list-style: none !important;
      list-style-type: none !important;
      background-image: none !important;
    }

    .user_reviews ul {
      font-size: 0.8em;
    }

    .user_reviews input {
      width: 79%;
    }

    .error {
      color: #cc0000;
    }

    .success {
      color: #339933;
    }

    .feoa_logout_link {
      display: block;
      font-size: 0.8em;
      margin-top: 7px;
    }
    .breather {
      line-height: 1.4em;
    }
    .nomargin {
      margin: 0px !important;
      padding: 0px !important;
    }

    .reviews {
      padding-bottom: 1.5em;;
    }

    .resource {
      padding-bottom: 12px;

    }

    .resource strong {
      line-height: 2em;
    }

    .orange {
      font-weight: 700;
      margin-top: 11px;
      color: darkorange;
    }

    .raty {
      margin-top: -6px;
    }
    .raty li {
      padding-bottom: 13px;;
    }

    .rattyLive {
      margin-top: 5px;
    }

    .error-message {
      background-color: #e4e9ec;
    }
  </style>
</head>
<body>
@if(true)
  <header class="masthead" role="banner" data-mobilemenu>
    <div class="masthead__top">
      <div class="inner">
        <a id="skiplinks" class="visuallyhidden focusable in-page" href="#main" tabindex="1">
          <span>Skip to main content</span>
        </a>
        <a class="masthead__logo" href="//jisc.ac.uk">
          <img alt="Jisc logo" src="{{url('/')}}/assets/img/Jisc_LogoAW_RGB.png" width="43" height="25">
        </a>
        <nav class="masthead__topnav">
          <ul id="nav" class="topnav__list" data-dropdown>
            <li class="topnav__item ">
              <a href="#">Digital Resources</a>
            </li>
            <li class="topnav__item  has-popup" data-dropdown-item>
              <a href="#">Journals</a>
              <div class="submenu">
                <ul>
                  <li><a href="#">Journal Archives</a></li>
                  <li><a href="#">Journal negotiations</a></li>
                  <li><a href="#">Journal Usage Statistics Portal</a></li>
                  <li><a href="#">Knowledge Base+</a></li>
                  <li><a href="#">The Keepers Registry</a></li>
                </ul>
                <aside>
                  <p>We negotiate with publishers to save you money on journal access,
                    and provide research, management and analysis services to
                    help you get the most from them.</p>
                </aside>
              </div>
            </li>
          </ul>
      </div>
    </div>
    <div class="masthead__main masthead__main--with-content">
      <div class="inner">
        <p class="masthead__title masthead__title--short"><a href="{{url('/')}}" title="FE Skills Onnile Academy">FES Online CPD Service</a></p>
        <div class="nav-wrapper">
          <nav class="masthead__nav header__nav--primary" role="navigation" data-dropdown>
            <ul>
              <li class="nav__item {{\Bentleysoft\Http\Controllers\ContentController::active($app->request, 'learn') }}">
                <a href="{{url('area/learn')}}">Learn</a>
              </li>
              <li class="nav__item {{\Bentleysoft\Http\Controllers\ContentController::active($app->request, 'find-and-reuse') }}">
                <a href="{{url('area/find-and-reuse')}}">Find and Reuse</a>
              </li>
              <li class="nav__item {{\Bentleysoft\Http\Controllers\ContentController::active($app->request, 'build') }}">
                <a href="{{url('area/build')}}">Build</a>
              </li>
              <li class="nav__item {{\Bentleysoft\Http\Controllers\ContentController::active($app->request, 'join') }}">
                <a href="{{url('join')}}">Join</a>
              </li>
            </ul>
          </nav>
          <nav class="masthead__nav masthead__nav--secondary" role="navigation" data-dropdown>
            <ul>
              <li class="nav__item {{\Bentleysoft\Http\Controllers\ContentController::active($app->request, 'login') }}">
                <a href="{{url('login')}}">Portal</a>
              </li>
              <li class="nav__item has-popup " data-dropdown-item>
                <a href="#">Support</a>
                <div class="submenu">
                  <ul>
                    <li><a href="#">Guides to the VLEs</a></li>
                    <li><a href="#">Embed</a></li>
                    <li><a href="#">Download and repackage</a></li>
                    <li><a href="#">Frequently asked questions</a></li>
                  </ul>
                </div>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </header>
  @yield('content')
  @include('partials.footer')

@endif

@if(false)
  @include('partials.orange.header')
    @yield('content')
  @include('partials.footer')
@endif


<!--script src="{{url('/')}}/assets/ux/js/ux.jisc-0.2.0.script-foot.min.js"></script-->
<!-- Insert Google Analytics here either using either Google Tag Manager, or by adding the tracking code directly onto every page. We include GA in this location on the page for performance reasons. -->
<script>
  $(document).ready(function(){
    $(".timed").delay(3200).fadeOut(300);
    $(".autoselect").change(function() {
          $('.search').submit();
        }
    );
  });
</script>
<script src="{{url('/')}}/assets/jquery/autocomplete/autodoodah.js"></script>
</body>
</html>
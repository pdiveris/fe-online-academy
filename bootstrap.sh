#!/usr/bin/env bash
echo "Running Petros's Astonishing ProvisionX ScriptY, developed in collaboration with Greg Speed and Torn Poster"

apt-get update
apt-get autoremove

sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password 18rockdove'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password 18rockdove'

apt-get install -y mysql-server 

apt-get remove -y apache2
apt-get install -y nginx


apt-get install -y php5
apt-get install -y php5-gd
apt-get install -y php5-mysql
apt-get install -y redis-server
apt-get install -y php5-redis
apt-get install -y php5-xdebug
apt-get install -y php5-fpm

apt-get install -y mc 

/etc/init.d/nginx stop
cp /vagrant/provision/conf/default /etc/nginx/sites-enabled/
/etc/init.d/nginx start


if ! [ -L /var/www/html ]; then
  rm -rf /var/www/html
  ln -fs /vagrant/public /var/www/html
fi

mysql -uroot -p18rockdove < /vagrant/provision/db/feoa_moodle.sql
